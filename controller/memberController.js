const bcrypt = require('bcrypt')
const jwt = require("jwt-simple")
const Member = require('../model/Member')
const Qrcode = require('../model/QRCode')
const File = require('../model/File')
const responseBuilder = require('../services/responseBuilder')
const stringHelper = require('../utils/stringHelper')
const mailSender = require('../services/mailSender')

module.exports = {
    authen: async (req, res) => {
        const { con, body, logger } = req
        
        try {
            const member = await Member.checkUserNameExist(con, body)
            
            if (member) {
                if (member.status !== 1) return res.json(responseBuilder.error('Incorrect username or password.'))

                const password = body.password + member.private_key
                const match = await bcrypt.compare(password, member.password)

                if (match) {
                    const payload = {
                        id: member.id,
                        iat: new Date().getTime(),
                        role: 'member',
                        application: 'mobile-application'
                    }
                    
                    return res.json(responseBuilder.success(jwt.encode(payload, process.env.JWT_SECRET_KEY)))

                }

                return res.json(responseBuilder.error('Incorrect username or password.'))
            } else {
                return res.json(responseBuilder.error('Incorrect username or password.'))
            }

        } catch (error) {
            logger.error('memberController -> authen -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong'))
        }
        
    },
    getProfile: (req, res) => {
        return res.json(responseBuilder.success({ result: req.user }))
    },
    checkUserNameExist: async (req, res) => {
        const { con, body, logger } = req

        try {
            const checkExistUserName = await Member.checkUserNameExist(con, body)

            if (checkExistUserName) {
                return res.json(responseBuilder.error('user is already exist.'))
            } else {
                return res.json(responseBuilder.success('user does not exist.'))
            }
        } catch (error) {
            logger.error('memberController -> checkUserNameExist -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong'))
        }
    },
    checkEmailExist: async (req, res) => {
        const { con, body, logger } = req

        try {
            const checkExistUserName = await Member.checkEmailExist(con, body)

            if (checkExistUserName) {
                return res.json(responseBuilder.error('Email is already exist.'))
            } else {
                return res.json(responseBuilder.success('Email does not exist.'))
            }
        } catch (error) {
            logger.error('memberController -> checkEmailExist -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong'))
        }
    },
    register: async (req, res) => {
        const { con, body, logger } = req

        try {
            const qrcodeItem = await Qrcode.getQRCodeItemById(con, { id: body.qrcodeItemId })

            if (!qrcodeItem) return res.json(responseBuilder.error('QRCode not found.'))
            if (qrcodeItem.usage_status !== 1) return res.json(responseBuilder.error('QRCode have been used.'))

            const checkExistUserName = await Member.checkUserNameExist(con, body)

            if (checkExistUserName) return res.json(responseBuilder.error('user is already exist.'))

            const member = await Member.register(con, body)

            if (member) {
                if (body.profileImageId) await File.setRefId(con, { id: body.profileImageId, refId: member[0] })
                if (body.locationImageId) await File.setRefId(con, { id: body.locationImageId, refId: member[0] })

                const payload = {
                    id: member[0],
                    iat: new Date().getTime(),
                    role: 'member',
                    application: 'mobile-application'
                }
                
                return res.json(responseBuilder.success(jwt.encode(payload, process.env.JWT_SECRET_KEY)))
                
            }  else {
                return res.json(responseBuilder.error('Registing was failed.'))
            }
        } catch (error) {
            logger.error('memberController -> register -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    update: async (req, res) => {
        const { con, body, user, logger } = req
        
        try {
            if (body.oldProfileImageId) {
                try {
                    await File.delete(con, { id: body.oldProfileImageId }, user)
                } catch (error) {
                    logger.error('memberController -> updateProfile -> delete old image -> ' + error.message)
                }
            }
            
            if (body.oldLocationImageId) {
                try {
                    await File.delete(con, { id: body.oldLocationImageId }, user)
                } catch (error) {
                    logger.error('memberController -> updateLocation -> delete old image -> ' + error.message)
                }
            }

            const member = await Member.update(con, body, user)
            if (member) {
                if (body.profileImageId) await File.setRefId(con, { id: body.profileImageId, refId: member[0] })
                if (body.locationImageId) await File.setRefId(con, { id: body.locationImageId, refId: member[0] })
            }
            return res.json(responseBuilder.success('Data have been updated.'))
        } catch (error) {
            logger.error('memberController -> update -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    updateProfile: async (req, res) => {
        const { con, body, user, logger } = req

        try {
            if (body.oldProfileImageId) {
                try {
                    // const deleteFile = await File.getFileById(con, { id: body.oldProfileImageId })
                    // if (deleteFile) {
                    await File.delete(con, { id: body.oldProfileImageId }, user)
                    //     const path = process.env.UPLOAD_PATH + '/' + deleteFile.path + '/' + deleteFile.name
                    //     fs.unlinkSync(path)
                    // }
                } catch (error) {
                    logger.error('memberController -> updateProfile -> delete old image -> ' + error.message)
                }
            }

            const member = await Member.updateProfile(con, body, user)
            if (member) {
                if (body.profileImageId) await File.setRefId(con, { id: body.profileImageId, refId: member[0] })
            }
            
            return res.json(responseBuilder.success('Data have been updated.'))
        } catch (error) {
            logger.error('memberController -> updateProfile -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    updateLocation: async (req, res) => {
        const { con, body, user, logger } = req

        try {
            if (body.oldLocationImageId) {
                try {
                    // const deleteFile = await File.getFileById(con, { id: body.oldLocationImageId })
                    // if (deleteFile) {
                    await File.delete(con, { id: body.oldLocationImageId }, user)
                        // const path = process.env.UPLOAD_PATH + '/' + deleteFile.path + '/' + deleteFile.name
                        // fs.unlinkSync(path)
                    // }
                } catch (error) {
                    logger.error('memberController -> updateLocation -> delete old image -> ' + error.message)
                }
            }

            const member = await Member.updateLocation(con, body, user)
           
            if (member) {
                if (body.locationImageId) await File.setRefId(con, { id: body.locationImageId, refId: member[0] })
            }

            return res.json(responseBuilder.success('Data have been updated.'))
        } catch (error) {
            logger.error('memberController -> updateLocation -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    changePasswordMember: async (req, res) => {
        const { con, body, user, logger } = req

        try {
            const member = await Member.getMemberByIdAllProps(con, body)
        
            if (member) {
                if (member.status !== 1) return res.json(responseBuilder.error('Account has been deleted'))
                await Member.changePasswordMember(con, body, user)
                return res.json(responseBuilder.success('Password has been changed'))
            } else {
                return res.json(responseBuilder.error('Member not found.'))
            }

        } catch (error) {
            logger.error('memberController -> changePasswordMember -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    changePassword: async (req, res) => {
        const { con, body, user, logger } = req

        try {
            const member = await Member.getMemberByIdAllProps(con, user)

            if (member) {
                if (member.status !== 1) return res.json(responseBuilder.error('Account has been deleted'))

                const password = body.currentPassword + member.private_key
                const match = await bcrypt.compare(password, member.password)

                if (match) {
                    
                    await Member.changePassword(con, body, user)
                    
                    // return res.json(responseBuilder.success(jwt.encode(payload, process.env.JWT_SECRET_KEY)))
                    return res.json(responseBuilder.success('Password has been changed'))
                } else {
                    return res.json(responseBuilder.error('Incorrect Current password.'))
                }
            } else {
                return res.json(responseBuilder.error('Member not found.'))
            }

        } catch (error) {
            logger.error('memberController -> changePassword -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    forgetPassword: async (req, res) => {
        const { con, body, logger } = req

        try {
            const member = await Member.getMemberByEmailAllProps(con, body)

            if (member) {
                if (member.status !== 1) return res.json(responseBuilder.error('Account has been deleted'))
                const newPassword = stringHelper.randomString(10)
                body.id = member.id
                body.newPassword = newPassword
                await Member.changePasswordMember(con, body, member)

                let html_text = 'QRCare <br>'
                html_text += `Hi, ${member.first_name} ${member.last_name} <br>`
                html_text += `We have already changed your password. <br>`
                html_text += `username: ${member.username} <br>`
                html_text += `password: ${newPassword} <br>`

                const params = {
                    from: 'qrcare.official2020@gmail.com',
                    to: body.email,
                    subject: 'Changing password has been successed',
                    text: html_text,
                    html: html_text
                }

                mailSender.sendMail(params)
                
                return res.json(responseBuilder.success('Password has been changed'))
            } else {
                return res.json(responseBuilder.error('Member not found.'))
            }

        } catch (error) {
            logger.error('memberController -> forgetPassword -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getMemberList: async (req, res) => {
        const { con, query, logger } = req

        try {   
            const items = await Member.getMemberList(con, query)
            const records = await Member.getMemberCount(con, query)
            return res.json(responseBuilder.success({ result: { totalRecords: Math.ceil(parseInt(records[0].count) / query.limit), items: items } }))
        } catch (error) {
            logger.error('memberController -> getMemberList -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getMemberById: async (req, res) => {
        const { con, params, logger } = req

        try {
            const member = await Member.getMemberById(con, params)
            if (member) {
                return res.json(responseBuilder.success({ result: member }))
            }
             
            return res.json(responseBuilder.error('Data not found.'))
        } catch (error) {
            logger.error('memberController -> getMemberById -> ' + error.message)
            return res.json(responseBuilder.error(error.message))
        }
    },
    delete: async (req, res) => {
        const { con, params, user, logger } = req
        
        try {
            const result = await Member.delete(con, params, user)

            if (result) {
                return res.json(responseBuilder.success({ result: 'Data have been deleted.' }))
            } else {
                return res.json(responseBuilder.error('Error, something wrong.'))
            }

        } catch (error) {
            logger.error('memberController -> delete -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    }
}