const fs = require('fs')
const FoodAllergy = require('../model/FoodAllergy')
const File = require('../model/File')
const responseBuilder = require('../services/responseBuilder')

module.exports = {
    add: async (req, res) => {
        const { con, body, user, logger } = req

        try {
            const foodAllergy = await FoodAllergy.add(con, body, user)
            
            if (foodAllergy) {
                if (body.imageId) await File.setRefId(con, { id: body.imageId, refId: foodAllergy[0] })

                return res.json(responseBuilder.success({ result: { id: foodAllergy[0] }}))
            } else {
                return res.json(responseBuilder.error('Error, create failed.'))
            }

        } catch (error) {
            logger.error('foodAllergyController -> add -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    update: async (req, res) => {
        const { con, body, user, logger } = req

        try {
            if (body.oldImageId) {
                try {
                    // const deleteFile = await File.getFileById(con, { id: body.oldImageId })
                    // if (deleteFile) {
                    await File.delete(con, { id: body.oldImageId }, user)
                        // const path = process.env.UPLOAD_PATH + '/' + deleteFile.path + '/' + deleteFile.name
                        // fs.unlinkSync(path)
                    // }
                } catch (error) {
                    logger.error('foodAllergyController -> update -> delete old image -> ' + error.message)
                }
            }

            const foodAllergy = await FoodAllergy.update(con, body, user)
            if (foodAllergy) {
                if (body.imageId) await File.setRefId(con, { id: body.imageId, refId: foodAllergy[0] })

                return res.json(responseBuilder.success({ result: { id: foodAllergy[0] }}))
            } else {
                return res.json(responseBuilder.error('Error, create failed.'))
            }
        } catch (error) {
            logger.error('foodAllergyController -> update -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getFoodAllergyList:  async (req, res) => {
        const { con, query, logger } = req
        
        try {
            const items = await FoodAllergy.getFoodAllergyList(con, query)
            const records = await FoodAllergy.getFoodAllergyCount(con, query)

            return res.json(responseBuilder.success({ result: { totalRecords: Math.ceil(parseInt(records[0].count) / query.limit), items: items } }))
        } catch (error) {
            logger.error('foodAllergyController -> getFoodAllergyList -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getFoodAllergyById: async (req, res) => {
        const { con, params, logger } = req
        
        try {
            const foodAllergy = await FoodAllergy.getFoodAllergyById(con, params)

            if (foodAllergy) {
                const image = await File.getFileByRefIdAndTag(con, { refId: foodAllergy.id, tag: 'food-allergy-image' })
                if (image) { 
                    foodAllergy.imageId = image.id
                    foodAllergy.image = image.path + '/' + image.name 
                }

                return res.json(responseBuilder.success({ result: foodAllergy }))
            } else {
                return res.json(responseBuilder.error('Data not found.'))
            }
            
        } catch (error) {
            logger.error('foodAllergyController -> getFoodAllergyById -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getFoodAllergyListByCreatureId: async (req, res) => {
        const { con, params, logger } = req

        try {
            const list = await FoodAllergy.getFoodAllergyListByCreatureId(con, params)
            
            if (list) {
                return res.json(responseBuilder.success({ result: list }))
            } else {
                return res.json(responseBuilder.error('Data not found.'))
            }
        } catch (error) {
            logger.error('foodAllergyController -> getFoodAllergyListByCreatureId -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    countFoodAllergyByCreatureId: async (req, res) => {
        const { con, params, logger } = req

        try {
            const count = await FoodAllergy.countFoodAllergyByCreatureId(con, params)
            
            if (count) {
                return res.json(responseBuilder.success({ result: count[0].count }))
            } else {
                return res.json(responseBuilder.error('Data not found.'))
            }
        } catch (error) {
            logger.error('foodAllergyController -> countFoodAllergyByCreatureId -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    delete: async (req, res) => {
        const { con, params, user, logger } = req
        
        try {
            const result = await FoodAllergy.delete(con, params, user)

            if (result) {
                return res.json(responseBuilder.success({ result: 'Data have been deleted.' }))
            } else {
                return res.json(responseBuilder.error('Error, something wrong.'))
            }

        } catch (error) {
            logger.error('foodAllergyController -> delete -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    deleteItems: async (req, res) => {
        const { con, body, user, logger } = req

        try {
            await FoodAllergy.deleteItems(con, { id: body.items }, user)
            return res.json(responseBuilder.success({ result: 'Data have been deleted.' }))
        } catch (error) {
            logger.error('foodAllergyController -> deleteItems -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    }
}