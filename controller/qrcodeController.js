
const puppeteer = require('puppeteer')
const File = require('../model/File')
const QRCode = require('../model/QRCode')
const Member = require('../model/Member')
const responseBuilder = require('../services/responseBuilder')

module.exports = {
    generate: async (req, res) => {
        const { con, body, user, logger } = req

        try {
            const qrcode =  await QRCode.generate(con, body, user)
            
            if (body.backgroundImageId) await File.setRefId(con, { id: body.backgroundImageId, refId: qrcode[0] })

            return res.json(responseBuilder.success({ result: 'success' }))
        } catch (error) {
            logger.error('qrcodeController -> generate -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getQRCodeSetList: async (req, res) => {
        const { con, query, logger } = req

        try {
            const items = await QRCode.getQRCodeSetList(con, query)
            const records = await QRCode.getQRCodeSetCount(con, query)
  
            return res.json(responseBuilder.success({ result: { totalRecords: Math.ceil(parseInt(records[0].count) / query.limit), items: items } }))
        } catch (error) {
            logger.error('qrcodeController -> getQRCodeSetList -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getQRCodeSetById: async (req, res) => {
        const { con, params, logger } = req

        try {
            let qrcodeSet = await QRCode.getQRCodeSetById(con, params)

            return res.json(responseBuilder.success({ result: qrcodeSet }))
        } catch (error) {
            logger.error('qrcodeController -> getQRCodeSetById -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    genetatePDF: (req, res) => {
        (async () => {
            const { con, params } = req
            const browser = await puppeteer.launch({ headless: true, args: ['--lang=th-TH, th'], args: ['--no-sandbox'] })
            
            const page = await browser.newPage()
            
            await page.goto(`https://qrcare-official.com/api/template/pattern/qrcode-set-pattern?id=${params.id}&ids=${params.ids}&size=${params.size}` )
            // await page.goto(`http://127.0.0.1:3000/api/template/pattern/qrcode-set-pattern?id=${params.id}&ids=${params.ids}&size=${params.size}` )
            const buffer = await page.pdf({ format: 'A4' })
            res.type('application/pdf')
            res.send(buffer)
            browser.close()
        })()
    },
    updateQRCodeSet: async (req, res) => {
        const { con, body, user, logger } = req

        try {
            if (body.oldBackgroundImageId) {
                try {
                    await File.delete(con, { id: body.oldBackgroundImageId }, user)
                } catch (error) {
                    logger.error('qrcodeController -> updateQRCodeSet -> delete old image -> ' + error.message)
                }
            }

            const qrcode = await QRCode.updateQRCodeSet(con, body, user)
            if (body.backgroundImageId) await File.setRefId(con, { id: body.backgroundImageId, refId: qrcode[0] })
            return res.json(responseBuilder.success({ result: 'success' }))
        } catch (error) {
            logger.error('qrcodeController -> updateQRCodeSet -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getQRCodeItemList: async (req, res) => {
        const { con, query, logger } = req

        try {
            const items = await QRCode.getQRCodeItemList(con, query)
            const records = await QRCode.getQRCodeItemCount(con, query)
  
            return res.json(responseBuilder.success({ result: { totalRecords: Math.ceil(parseInt(records[0].count) / query.limit), items: items } }))
        } catch (error) {
            logger.error('qrcodeController -> getQRCodeItemList -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getQRCodeItemById: async (req, res) => {
        const { con, params, query, logger } = req

        try {
            const qrcodeItem = await QRCode.getQRCodeItemById(con, params)

            if (qrcodeItem) {
                if (query.get_children === '1') {
                    const member = await Member.getMemberByQRCodeItemId(con, { qrcodeItemId: qrcodeItem.id })

                    if (member) {   
                        qrcodeItem = {
                            ...qrcodeItem,
                            member: member
                        }
                    }
                } 

                return res.json(responseBuilder.success({ result: qrcodeItem }))
                
            } else {
                return res.json(responseBuilder.error('Data not found'))
            }

        } catch (error) {
            logger.error('qrcodeController -> getQRCodeItemById -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getQRCodeInfo: async (req, res) => {
        const { con, query, logger } = req

        try {
            const total = await QRCode.countQRCodeAll(con)
            const used = await QRCode.countQRCodeUsed(con)
  
            return res.json(responseBuilder.success({ result: { total: total[0].count, used: used[0].count } }))
        } catch (error) {
            logger.error('qrcodeController -> getQRCodeSetList -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    }
}