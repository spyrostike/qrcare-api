const _ = require('lodash')
const dayjs = require('dayjs')
const TreatmentHistory = require('../model/TreatmentHistory')
const File = require('../model/File')
const Mail = require('../model/Mail')
const responseBuilder = require('../services/responseBuilder')

module.exports = {
    add: async (req, res) => {
        const { con, body, user, logger } = req

        try {
            const treatmentHistory = await TreatmentHistory.add(con, body, user)
            
            if (treatmentHistory) {
                if (body.imageId) await File.setRefId(con, { id: body.imageId, refId: treatmentHistory[0] })

                return res.json(responseBuilder.success({ result: { id: treatmentHistory[0] }}))
            } else {
                return res.json(responseBuilder.error('Error, create failed.'))
            }

        } catch (error) {
            logger.error('treatmentHistoryController -> add -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    update: async (req, res) => {
        const { con, body, user, logger } = req

        try {
            if (body.oldImageId) {
                
                try {
                    // const deleteFile = await File.getFileById(con, { id: body.oldImageId })
                    // if (deleteFile) {
                    await File.delete(con, { id: body.oldImageId }, user)
                        // const path = process.env.UPLOAD_PATH + '/' + deleteFile.path + '/' + deleteFile.name
                        // fs.unlinkSync(path)
                    // }
                } catch (error) {
                    logger.error('treatmentHistoryController -> update -> delete old image -> ' + error.message)
                }
            }

            const treatmentHistory = await TreatmentHistory.update(con, body, user)
            if (treatmentHistory) {
                if (body.imageId) await File.setRefId(con, { id: body.imageId, refId: treatmentHistory[0] })

                return res.json(responseBuilder.success({ result: { id: treatmentHistory[0] }}))
            } else {
                return res.json(responseBuilder.error('Error, create failed.'))
            }
        } catch (error) {
            logger.error('treatmentHistoryController -> update -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getTreatmentHistoryById: async (req, res) => {
        const { con, params, logger } = req

        try {
            const treatmentHistory = await TreatmentHistory.getTreatmentHistoryById(con, params)

            if (treatmentHistory) {
                const image = await File.getFileByRefIdAndTag(con, { refId: treatmentHistory.id, tag: 'treatment-history-image' })
 
                if (image) { 
                    treatmentHistory.imageId = image.id
                    treatmentHistory.image = image.path + '/' + image.name 
                }

                const count = await File.getFileCountByRefIdAndTag(con, { refId: treatmentHistory.id, tag: 'treatment-history-image-list' })

                if (count) { 
                    treatmentHistory.imageCount = count[0].count
                }

                return res.json(responseBuilder.success({ result: treatmentHistory }))
            } else {
                return res.json(responseBuilder.error('Data not found.'))
            }
            
        } catch (error) {
            logger.error('treatmentHistoryController -> getTreatmentHistoryById -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getTreatmentHistoryList:  async (req, res) => {
        const { con, query, logger } = req
     
        try {
            const items = await TreatmentHistory.getTreatmentHistoryList(con, query)
            const records = await TreatmentHistory.getTreatmentHistoryCount(con, query)
            return res.json(responseBuilder.success({ result: { totalRecords: Math.ceil(parseInt(records[0].count) / query.limit), items: items } }))
        } catch (error) {
            logger.error('treatmentHistoryController -> getTreatmentHistoryList -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getTreatmentHistoryListByCreatureId: async (req, res) => {
        const { con, params, logger } = req

        try {
            const list = await TreatmentHistory.getTreatmentHistoryListByCreatureId(con, params)
            
            if (list) {
                return res.json(responseBuilder.success({ result: list }))
            } else {
                return res.json(responseBuilder.error('Data not found.'))
            }
        } catch (error) {
            logger.error('treatmentHistoryController -> getTreatmentHistoryListByCreatureId -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    delete: async (req, res) => {
        const { con, params, user, logger } = req
        
        try {
            const result = await TreatmentHistory.delete(con, params, user)

            if (result) {
                return res.json(responseBuilder.success({ result: 'Data have been deleted.' }))
            } else {
                return res.json(responseBuilder.error('Error, something wrong.'))
            }

        } catch (error) {
            logger.error('treatmentHistoryController -> delete -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    deleteItems: async (req, res) => {
        const { con, body, user, logger } = req

        try {
           
            await TreatmentHistory.deleteItems(con, { id: body.items }, user)

            return res.json(responseBuilder.success({ result: 'Data have been deleted.' }))

        } catch (error) {
            logger.error('treatmentHistoryController -> deleteItems -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    sendMail: async (req, res) => {
        const { con, body, user, logger } = req

        try {
            const items = await TreatmentHistory.getTreatmentHistoryListWhereIn(con, body, user)
            
            var result = _.chain(items).groupBy('id').map((item, key) => { 
                
                const first = _.first(item)

                const images = _.filter(item, image => image.image_path_si && image.image_name_si).map(temp => ({ image_path: temp.image_path_si, image_name: temp.image_name_si}))

                return {
                    ...first,
                    images: images
                }
            }).valueOf()
            
            let html_text = 'Detail<br>'
            html_text += `From ${body.from}`
            html_text += body.description ? body.description + '<br>' : ''

            for (var i = 0;i < result.length;i++) {
                let text = "****************************************************************************************"
                text = 'Treatment history Number: ' + (i + 1) + '.<br>'
                text += `<img src="${process.env.API_IMAGE_PATH + '/' + result[i].image_path + '/' + result[i].image_name}" width="280" ><br>`
                text += 'Department: ' + result[i].department_name + '<br>'
                text += 'Description: ' + result[i].description + '<br>'
                text += 'File category: ' + result[i].department_name + '<br>'
                text += 'Date: ' + dayjs(result[i].date).format('D/MM/YYYY') + ' ' + result[i].time + '<br>'
                
                for (var j = 0;j < result[i].images.length;j++) {
                    text += `<img src="${process.env.API_IMAGE_PATH + '/' + result[i].images[j].image_path + '/' + result[i].images[j].image_name}" width="480" height="480"><br>`
                }
                text += "****************************************************************************************"

                html_text += text
            }

            const params = {
                from: body.from,
                to: body.to,
                subject: body.subject,
                description: body.description,
                tag: body.tag,
                html_text: html_text
            }

            await Mail.add(con, params, user)
            return res.json(responseBuilder.success({ result: 'Email have been sent successfully.' }))
            
        } catch (error) {
            logger.error('treatmentHistoryController -> sendMail -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    }
}