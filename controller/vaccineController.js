const File = require('../model/File')
const dayjs = require('dayjs')
const Vaccine = require('../model/Vaccine')
const responseBuilder = require('../services/responseBuilder')

module.exports = {
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////// vaccine book ////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////

    addBook: async (req, res) => {
        const { con, body, user, logger } = req

        try {
            const id = await Vaccine.addBook(con, body, user)
            return res.json(responseBuilder.success({ result: { id: id[0] } }))
        } catch (error) {
            logger.error('vaccineController -> addBook -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    updateBook: async (req, res) => {
        const { con, body, user, logger } = req

        try {
            await Vaccine.updateBook(con, body, user)
            
            return res.json(responseBuilder.success({ result: 'Data have been updated.' }))
        } catch (error) {
            logger.error('vaccineController -> updateBook -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getVaccineBookListByCreatureId: async (req, res) => {
        const { con, params, logger } = req

        try {
            const vaccineBook = await Vaccine.getVaccineBookListByCreatureId(con, params)
            return res.json(responseBuilder.success({ result: vaccineBook }))
        } catch (error) {
            logger.error('vaccineController -> getVaccineBookListByCreatureId -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getVaccineBookList:  async (req, res) => {
        const { con, query, logger } = req

        try {
            const items = await Vaccine.getVaccineBookList(con, query)
            const records = await Vaccine.getVaccineBookCount(con, query)
            return res.json(responseBuilder.success({ result: { totalRecords: Math.ceil(parseInt(records[0].count) / query.limit), items: items } }))
        } catch (error) {
            logger.error('vaccineController -> getVaccineBookList -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getVaccineBookById: async (req, res) => {
        const { con, params, logger } = req
        
        try {
            const vaccine = await Vaccine.getVaccineBookById(con, params)
            
            if (vaccine) {
                return res.json(responseBuilder.success({ result: vaccine }))
            } else {
                return res.json(responseBuilder.error('Data not found.'))
            }
        } catch (error) {
            logger.error('vaccineController -> getVaccineBookById -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    deleteBook: async (req, res) => {
        const { con, params, user, logger } = req
        
        try {
            const result = await Vaccine.deleteBook(con, params, user)

            if (result) {
                return res.json(responseBuilder.success({ result: 'Data have been deleted.' }))
            } else {
                return res.json(responseBuilder.error('Error, something wrong.'))
            }

        } catch (error) {
            logger.error('vaccineController -> deleteBook -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    deleteBookItems: async (req, res) => {
        const { con, body, user, logger } = req

        try {
            await Vaccine.deleteBookItems(con, { id: body.items }, user)
            return res.json(responseBuilder.success({ result: 'Data have been deleted.' }))
        } catch (error) {
            logger.error('vaccineController -> deleteBookItems -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },

    ///////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////// vaccine ///////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////

    add: async (req, res) => {
        const { con, body, user, logger } = req

        try {
            const vaccine = await Vaccine.add(con, body, user)
            
            if (vaccine) {
                if (body.imageId) await File.setRefId(con, { id: body.imageId, refId: vaccine[0] })

                return res.json(responseBuilder.success({ result: { id: vaccine[0] }}))
            } else {
                return res.json(responseBuilder.error('Error, create failed.'))
            }

        } catch (error) {
            logger.error('VaccineController -> add -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    update: async (req, res) => {
        const { con, body, user, logger } = req

        try {
            if (body.oldImageId) {
                
                try {
                    await File.delete(con, { id: body.oldImageId }, user)
                } catch (error) {
                    logger.error('VaccineController -> update -> delete old image -> ' + error.message)
                }
            }
            const vaccine = await Vaccine.update(con, body, user)
            if (vaccine) {
                if (body.imageId) await File.setRefId(con, { id: body.imageId, refId: vaccine[0] })

                return res.json(responseBuilder.success({ result: { id: vaccine[0] }}))
            } else {
                return res.json(responseBuilder.error('Error, create failed.'))
            }
        } catch (error) {
            logger.error('VaccineController -> update -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    delete: async (req, res) => {
        const { con, params, user, logger } = req
        
        try {
            const result = await Vaccine.delete(con, params, user)

            if (result) {
                return res.json(responseBuilder.success({ result: 'Data have been deleted.' }))
            } else {
                return res.json(responseBuilder.error('Error, something wrong.'))
            }

        } catch (error) {
            logger.error('vaccineController -> delete -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getVaccineListByBookId: async (req, res) => {
        const { con, params, logger } = req
    
        try {
            const vaccineList = await Vaccine.getVaccineListByBookId(con, params)
            return res.json(responseBuilder.success({ result: vaccineList }))
        } catch (error) {
            logger.error('vaccineController -> getVaccineListByBookId -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getVaccineList: async (req, res) => {
        const { con, query, logger } = req
    
        try {
            const items = await Vaccine.getVaccineList(con, query)
            const records = await Vaccine.getVaccineCount(con, query)
            return res.json(responseBuilder.success({ result: { totalRecords: Math.ceil(parseInt(records[0].count) / query.limit), items: items } }))
        } catch (error) {
            logger.error('vaccineController -> getVaccineList -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getVaccineById: async (req, res) => {
        const { con, params, logger } = req

        try {
            const vaccine = await Vaccine.getVaccineById(con, params)

            if (vaccine) {
                const image = await File.getFileByRefIdAndTag(con, { refId: vaccine.id, tag: 'vaccine-image' })
 
                if (image) { 
                    vaccine.imageId = image.id
                    vaccine.image = image.path + '/' + image.name 
                }

                return res.json(responseBuilder.success({ result: vaccine }))
            } else {
                return res.json(responseBuilder.error('Data not found.'))
            }
            
        } catch (error) {
            logger.error('vaccineController -> getVaccineById -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    }
}