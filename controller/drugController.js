const fs = require('fs')
const Drug = require('../model/Drug')
const File = require('../model/File')
const responseBuilder = require('../services/responseBuilder')

module.exports = {
    add: async (req, res) => {
        const { con, body, user, logger } = req

        try {
            const drug = await Drug.add(con, body, user)
            
            if (drug) {
                if (body.imageId) await File.setRefId(con, { id: body.imageId, refId: drug[0] })

                return res.json(responseBuilder.success({ result: { id: drug[0] }}))
            } else {
                return res.json(responseBuilder.error('Error, create failed.'))
            }

        } catch (error) {
            logger.error('drugController -> add -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    update: async (req, res) => {
        const { con, body, user, logger } = req

        try {
            if (body.oldImageId) {
                try {
                    // const deleteFile = await File.getFileById(con, { id: body.oldImageId })
                    // if (deleteFile) {
                    await File.delete(con, { id: body.oldImageId }, user)
                    //     const path = process.env.UPLOAD_PATH + '/' + deleteFile.path + '/' + deleteFile.name
                    //     fs.unlinkSync(path)
                    // }
                } catch (error) {
                    logger.error('drugController -> update -> delete old image -> ' + error.message)
                }
            }

            const drug = await Drug.update(con, body, user)
            if (drug) {
                if (body.imageId) await File.setRefId(con, { id: body.imageId, refId: drug[0] })

                return res.json(responseBuilder.success({ result: { id: drug[0] }}))
            } else {
                return res.json(responseBuilder.error('Error, create failed.'))
            }
        } catch (error) {
            logger.error('drugController -> update -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getDrugList:  async (req, res) => {
        const { con, query, logger } = req

        try {
            const items = await Drug.getDrugList(con, query)
            const records = await Drug.getDrugCount(con, query)

            return res.json(responseBuilder.success({ result: { totalRecords: Math.ceil(parseInt(records[0].count) / query.limit), items: items } }))
        } catch (error) {
            logger.error('drugController -> getDrugList -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getDrugById: async (req, res) => {
        const { con, params, logger } = req

        try {
            const drug = await Drug.getDrugById(con, params)

            if (drug) {
                const image = await File.getFileByRefIdAndTag(con, { refId: drug.id, tag: 'drug-image' })

                if (image) { 
                    drug.imageId = image.id
                    drug.image = image.path + '/' + image.name 
                }

                return res.json(responseBuilder.success({ result: drug }))
            } else {
                return res.json(responseBuilder.error('Data not found.'))
            }
            
        } catch (error) {
            logger.error('drugController -> getDrugById -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getDrugListByCreatureId: async (req, res) => {
        const { con, params, logger } = req

        try {
            const list = await Drug.getDrugListByCreatureId(con, params)
            
            if (list) {
                return res.json(responseBuilder.success({ result: list }))
            } else {
                return res.json(responseBuilder.error('Data not found.'))
            }
        } catch (error) {
            logger.error('drugController -> getDrugListByCreatureId -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    countDrugByCreatureId: async (req, res) => {
        const { con, params, logger } = req
        
        try {
            const count = await Drug.countDrugByCreatureId(con, params)
            
            if (count) {
                return res.json(responseBuilder.success({ result: count[0].count }))
            } else {
                return res.json(responseBuilder.error('Data not found.'))
            }
        } catch (error) {
            logger.error('drugController -> countDrugByCreatureId -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    delete: async (req, res) => {
        const { con, params, user, logger } = req
        
        try {
            const result = await Drug.delete(con, params, user)

            if (result) {
                return res.json(responseBuilder.success({ result: 'Data have been deleted.' }))
            } else {
                return res.json(responseBuilder.error('Error, something wrong.'))
            }

        } catch (error) {
            logger.error('drugController -> delete -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    deleteItems: async (req, res) => {
        const { con, body, user, logger } = req

        try {
            await Drug.deleteItems(con, { id: body.items }, user)
            return res.json(responseBuilder.success({ result: 'Data have been deleted.' }))
        } catch (error) {
            logger.error('treatmentHistoryController -> deleteItems -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    }
}