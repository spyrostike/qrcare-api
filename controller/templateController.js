const dayjs = require('dayjs')
const QRCodeLib = require('qrcode')
const _ = require('lodash')
const QRCode = require('../model/QRCode')

module.exports = {
    qrcodeSetPattern: async (req, res) => {
        const { con, query, logger } = req

        let ids = null

        if (query.ids === 'all') {

        } else {
            ids = JSON.parse(Buffer.from(query.ids, 'base64').toString())
        }
        
        
        try {
            let qrcodeSet = await QRCode.getQRCodeSetById(con, query)
            
            if (qrcodeSet) {
                let qrcodeItems = null
                if (query.ids === 'all') {
                    qrcodeItems = await QRCode.getQRCodeItemsByParentId(con, { parentId: query.id })
                } else {
                    qrcodeItems = await QRCode.getQRCodeItemsByIds(con, { ids: ids })
                }

                items = await generateQRCode(qrcodeItems, query.size)

                    qrcodeSet = {
                        ...qrcodeSet,
                        items: items
                    }

                    const convertJSONDateToString = (value) => {
                        return dayjs(value).format('D/MM/YYYY')
                    }

                    const templateData = { 
                        parent: qrcodeSet, 
                        convertJSONDateToString: convertJSONDateToString,
                        size: query.size,
                        logoImage: `${process.env.API_IMAGE_PATH}/images/logo_profile_qr.png`,
                        backgroundImage: qrcodeSet.background_image_path && qrcodeSet.background_image_name ? process.env.API_IMAGE_PATH + '/' + qrcodeSet.background_image_path + '/' + qrcodeSet.background_image_name : '/asset/images/complete_background.png'
                    }

                    return res.render('../views/pattern/qrcode-set', templateData)
            } else {
                return res.status(404).send('404 Not found')
            }
        } catch (error) {
            logger.error('templateController -> qrcodeSetPattern -> ' + error.message)
            return res.status(404).send('404 Not found')
        }
    }
}

const generateQRCode = (items, size) => {
    return new Promise(async (resolve, reject) => { 
        for (var i = 0; i < items.length ; i++) {
            const qrcode = await generateQR(process.env.DOMAIN_URL + '/creature/' +  items[i].id)
            items[i].qrcode = qrcode
        }
        
        if (size === '2') {
            resolve(_.chunk( _.chunk(items, size), 3))
        } else if (size === '3') {
            resolve(_.chunk( _.chunk(items, size), 3))
        }
        
    })
}

const generateQR = async text => {
    try {
        return await QRCodeLib.toDataURL(text, {
            color: {
                dark: '#000',  // Blue dots
                light: '#0000' // Transparent background
            }
        })
    } catch (err) {
      console.error(err)
    }
}