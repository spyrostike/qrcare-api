const fs = require('fs')
const File = require('../model/File')
const responseBuilder = require('../services/responseBuilder')

module.exports = {
    add: async (req, res) => {
        let { con, body, file, logger } = req
        
        try {
            if (file) {
                body.filename = file.filename
                body.mimetype = file.mimetype
                body.size = file.size
                body.destination = file.destination

                const addFile = await File.add(con, body)
                return res.json(responseBuilder.success({ result: addFile[0] }))
            }

            return res.json(responseBuilder.error('File not found.'))
        } catch (error) {
            logger.error('fileController -> add -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    setFilesRefId: async (req, res) => {
        let { con, body, logger } = req
        console.log('sdsdb', body)
        try {
            await File.setFilesRefId(con, body)
            return res.json(responseBuilder.success({ result: 'Data have been updated.' }))
        } catch (error) {
            logger.error('fileController -> setFilesRefId -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getFileList: async (req, res) => {
        let { con, query, logger } = req

        try {
            const list = await File.getFileList(con, query)
            const records = await File.getFileCountByRefIdAndTag(con, query)
            if (list) {
                return res.json(responseBuilder.success({ result: { items: list, totalRecords: records[0].count } }))
            } else {
                return res.json(responseBuilder.error('Data not found.'))
            }
        } catch (error) {
            logger.error('fileController -> getFileList -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    delete: async (req, res) => {
        const { con, params, user, logger } = req
        try {
            const result = await File.delete(con, params, user)

            if (result) {
                return res.json(responseBuilder.success({ result: 'Data have been deleted.' }))
            } else {
                return res.json(responseBuilder.error('Error, something wrong.'))
            }

        } catch (error) {
            logger.error('fileController -> delete -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    deleteItems: async (req, res) => {
        const { con, body, user, logger } = req

        try {
            await File.deleteItems(con, { id: body.items }, user)
            return res.json(responseBuilder.success({ result: 'Data have been deleted.' }))
        } catch (error) {
            logger.error('treatmentHistoryController -> deleteItems -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    }
}