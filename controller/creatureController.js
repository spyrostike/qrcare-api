const fs = require('fs')
const Creature = require('../model/Creature')
const Medical = require('../model/Medical')
const Qrcode = require('../model/QRCode')
const File = require('../model/File')
const responseBuilder = require('../services/responseBuilder')


module.exports = {
    add: async (req, res) => {
        const { con, body, user, logger } = req

        try {
            const qrcodeItem =  await Qrcode.getQRCodeItemById(con, { id: body.qrcodeItemId })

            if (!qrcodeItem) return res.json(responseBuilder.error('QRCode not found.'))
            if (qrcodeItem.usage_status === 3 ) return res.json(responseBuilder.error('QRCode have been used.'))

            const creature = await Creature.add(con, body, user)
            
            if (creature) {
                if (body.creatureImageId) await File.setRefId(con, { id: body.creatureImageId, refId: creature[0] })
                if (body.birthCertificateId) await File.setRefId(con, { id: body.birthCertificateId, refId: creature[0] })

                return res.json(responseBuilder.success({ result: { id: creature[0] }}))
            } else {
                return res.json(responseBuilder.error('Error, create failed.'))
            }
        } catch (error) {
            logger.error('creatureController -> add -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    update: async (req, res) => {
        const { con, body, user, logger } = req
        
        try {
            if (body.oldCreatureImageId) {
                try {
                    await File.delete(con, { id: body.oldCreatureImageId }, user)
                } catch (error) {
                    logger.error('creatureController -> update -> delete old creature image -> ' + error.message)
                }
            }

            if (body.oldBirthCertificate) {
                try {
                    await File.delete(con, { id: body.oldBirthCertificate }, user)
                } catch (error) {
                    logger.error('creatureController -> update -> delete old birth-certificate -> ' + error.message)
                }
            }
            
            const creature = await Creature.update(con, body, user)
            
            if (creature) {
                if (body.creatureImageId) await File.setRefId(con, { id: body.creatureImageId, refId: creature[0] })
                if (body.birthCertificateId) await File.setRefId(con, { id: body.birthCertificateId, refId: creature[0] })

                const medical = await Medical.getMedicalByCreatureId(con, { creatureId: body.id })
                
                if (medical) Medical.update(con, { bloodType: body.bloodType, id: medical.id }, user)

                return res.json(responseBuilder.success({ result: { id: creature[0] }}))
            } else {
                return res.json(responseBuilder.error('Error, create failed.'))
            }
        } catch (error) {
            logger.error('creatureController -> update -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getCreatureById: async (req, res) => {
        const { con, params, logger } = req

        try {
            const creature = await Creature.getCreatureById(con, params)
            
            if (creature) {
                const creatureImage = await File.getFileByRefIdAndTag(con, { refId: creature.id, tag: 'creature-profile-image' })
                
                if (creatureImage) { 
                    creature.creatureImageId = creatureImage.id
                    creature.creatureImage = creatureImage.path + '/' + creatureImage.name 
                }

                const birthCertificate = await File.getFileByRefIdAndTag(con, { refId: creature.id, tag: 'creature-birth-certificate-image' })
                if (birthCertificate) { 
                    creature.birthCertificateId = birthCertificate.id
                    creature.birthCertificate = birthCertificate.path  + '/' + birthCertificate.name
                }

                return res.json(responseBuilder.success({ result: creature }))
            } else {
                return res.json(responseBuilder.error('Data not found.'))
            }
        } catch (error) {
            logger.error('creatureController -> getCreatureById -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getCreatureByQRCodeId: async (req, res) => {
        const { con, params, logger } = req

        try {
            const creature = await Creature.getCreatureByQRCodeId(con, params)

            if (creature) {
                const creatureImage = await File.getFileByRefIdAndTag(con, { refId: creature.id, tag: 'creature-profile-image' })
                
                if (creatureImage) { 
                    creature.creatureImageId = creatureImage.id
                    creature.creatureImage = creatureImage.path + '/' + creatureImage.name 
                }

                const birthCertificate = await File.getFileByRefIdAndTag(con, { refId: creature.id, tag: 'creature-birth-certificate-image' })
                if (birthCertificate) { 
                    creature.birthCertificateId = birthCertificate.id
                    creature.birthCertificate = birthCertificate.path  + '/' + birthCertificate.name
                }
                
                return res.json(responseBuilder.success({ result: creature }))
            } else {
                return res.json(responseBuilder.error('Data not found.'))
            }
        } catch (error) {
            logger.error('creatureController -> getCreatureByQRCodeId -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getCreatureListByMemberId: async (req, res) => {
        const { con, params, user, logger } = req

        try {
            const result = await Creature.getCreatureListByMemberId(con, params, user)

            if (result) {
                return res.json(responseBuilder.success({ result: result }))
            } else {
                return res.json(responseBuilder.success({ result: [] }))
            }
        } catch (error) {
            logger.error('creatureController -> getCreatureListByMemberId -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getCreatureList:  async (req, res) => {
        const { con, query, logger } = req

        try {
            const items = await Creature.getCreatureList(con, query)
            const records = await Creature.getCreatureCount(con, query)

            return res.json(responseBuilder.success({ result: { totalRecords: Math.ceil(parseInt(records[0].count) / query.limit), items: items } }))
        } catch (error) {
            logger.error('creatureController -> getCreatureList -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    delete: async (req, res) => {
        const { con, params, user, logger } = req
   
        try {
            const result = await Creature.delete(con, params, user)

            if (result) {
                return res.json(responseBuilder.success({ result: 'Data have been deleted.' }))
            } else {
                return res.json(responseBuilder.error('Error, something wrong.'))
            }
        } catch (error) {
            logger.error('creatureController -> delete -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    deleteItems: async (req, res) => {
        const { con, body, user, logger } = req

        try {
            await Creature.deleteItems(con, { id: body.items }, user)
            return res.json(responseBuilder.success({ result: 'Data have been deleted.' }))
        } catch (error) {
            logger.error('creatureController -> deleteItems -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    }
}