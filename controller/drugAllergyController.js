const fs = require('fs')
const DrugAllergy = require('../model/DrugAllergy')
const File = require('../model/File')
const responseBuilder = require('../services/responseBuilder')

module.exports = {
    add: async (req, res) => {
        const { con, body, user, logger } = req

        try {
            const drugAllergy = await DrugAllergy.add(con, body, user)
            
            if (drugAllergy) {
                if (body.imageId) await File.setRefId(con, { id: body.imageId, refId: drugAllergy[0] })

                return res.json(responseBuilder.success({ result: { id: drugAllergy[0] }}))
            } else {
                return res.json(responseBuilder.error('Error, create failed.'))
            }

        } catch (error) {
            logger.error('drugAllergyController -> add -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    update: async (req, res) => {
        const { con, body, user, logger } = req

        try {
            if (body.oldImageId) {
                try {
                    // const deleteFile = await File.getFileById(con, { id: body.oldImageId })
                    // if (deleteFile) {
                    await File.delete(con, { id: body.oldImageId }, user)
                    //     const path = process.env.UPLOAD_PATH + '/' + deleteFile.path + '/' + deleteFile.name
                    //     fs.unlinkSync(path)
                    // }
                } catch (error) {
                    logger.error('drugAllergyController -> update -> delete old image -> ' + error.message)
                }
            }

            const drugAllergy = await DrugAllergy.update(con, body, user)
            if (drugAllergy) {
                if (body.imageId) await File.setRefId(con, { id: body.imageId, refId: drugAllergy[0] })

                return res.json(responseBuilder.success({ result: { id: drugAllergy[0] }}))
            } else {
                return res.json(responseBuilder.error('Error, create failed.'))
            }
        } catch (error) {
            logger.error('drugAllergyController -> update -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getDrugAllergyList:  async (req, res) => {
        const { con, query, logger } = req

        try {
            const items = await DrugAllergy.getDrugAllergyList(con, query)
            const records = await DrugAllergy.getDrugAllergyCount(con, query)

            return res.json(responseBuilder.success({ result: { totalRecords: Math.ceil(parseInt(records[0].count) / query.limit), items: items } }))
        } catch (error) {
            logger.error('drugAllergyController -> getDrugAllergyList -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getDrugAllergyById: async (req, res) => {
        const { con, params, logger } = req

        try {
            const drugAllergy = await DrugAllergy.getDrugAllergyById(con, params)

            if (drugAllergy) {
                const image = await File.getFileByRefIdAndTag(con, { refId: drugAllergy.id, tag: 'drug-allergy-image' })
                if (image) { 
                    drugAllergy.imageId = image.id
                    drugAllergy.image = image.path + '/' + image.name 
                }

                return res.json(responseBuilder.success({ result: drugAllergy }))
            } else {
                return res.json(responseBuilder.error('Data not found.'))
            }
            
        } catch (error) {
            logger.error('drugAllergyController -> getDrugAllergyById -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    getDrugAllergyListByCreatureId: async (req, res) => {
        const { con, params, logger } = req

        try {
            const list = await DrugAllergy.getDrugAllergyListByCreatureId(con, params)
            
            if (list) {
                return res.json(responseBuilder.success({ result: list }))
            } else {
                return res.json(responseBuilder.error('Data not found.'))
            }
        } catch (error) {
            logger.error('drugAllergyController -> getDrugAllergyListByCreatureId -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    countDrugAllergyByCreatureId: async (req, res) => {
        const { con, params, logger } = req
        
        try {
            const count = await DrugAllergy.countDrugAllergyByCreatureId(con, params)
            
            if (count) {
                return res.json(responseBuilder.success({ result: count[0].count }))
            } else {
                return res.json(responseBuilder.error('Data not found.'))
            }
        } catch (error) {
            logger.error('drugAllergyController -> countDrugAllergyByCreatureId -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    delete: async (req, res) => {
        const { con, params, user, logger } = req
        
        try {
            const result = await DrugAllergy.delete(con, params, user)

            if (result) {
                return res.json(responseBuilder.success({ result: 'Data have been deleted.' }))
            } else {
                return res.json(responseBuilder.error('Error, something wrong.'))
            }

        } catch (error) {
            logger.error('drugAllergyController -> delete -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    },
    deleteItems: async (req, res) => {
        const { con, body, user, logger } = req

        try {
            await DrugAllergy.deleteItems(con, { id: body.items }, user)
            return res.json(responseBuilder.success({ result: 'Data have been deleted.' }))
        } catch (error) {
            logger.error('drugAllergyController -> deleteItems -> ' + error.message)
            return res.json(responseBuilder.error('Error, something wrong.'))
        }
    }
}