const express = require('express')
const foodAllergyController = require('../controller/foodAllergyController')
const jwtHelper = require('../services/jwtHelper')

const router = express.Router()

router.post('/add', jwtHelper.requireJWTAuth, foodAllergyController.add)
router.put('/update', jwtHelper.requireJWTAuth, foodAllergyController.update)
router.get('/list/:creature_id', foodAllergyController.getFoodAllergyListByCreatureId)
router.get('/get-food-allergy-list', jwtHelper.requireJWTAuth, foodAllergyController.getFoodAllergyList)
router.get('/:id', foodAllergyController.getFoodAllergyById)
router.get('/count/:creature_id', foodAllergyController.countFoodAllergyByCreatureId)
router.delete('/delete/:id', jwtHelper.requireJWTAuth, foodAllergyController.delete)
router.put('/delete', jwtHelper.requireJWTAuth, foodAllergyController.deleteItems)

module.exports = router