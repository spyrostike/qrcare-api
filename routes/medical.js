const express = require('express')
const medicalController = require('../controller/medicalController')
const jwtHelper = require('../services/jwtHelper')

const router = express.Router()

router.post('/add', jwtHelper.requireJWTAuth, medicalController.add)
router.put('/update', jwtHelper.requireJWTAuth, medicalController.update)
router.get('/get-medical-by-creature-id/:creatureId', medicalController.getMedicalByCreatureId)
module.exports = router