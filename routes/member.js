const express = require('express')
const memberController = require('../controller/memberController')
const jwtHelper = require('../services/jwtHelper')

const router = express.Router()

router.post('/authen', memberController.authen)
router.get('/profile', jwtHelper.requireJWTAuth, memberController.getProfile)
router.get('/get-member-list', jwtHelper.requireJWTAuth, memberController.getMemberList)
router.get('/:id', memberController.getMemberById)
router.post('/check-username-exist', memberController.checkUserNameExist)
router.post('/check-email-exist', memberController.checkEmailExist)
router.post('/register', memberController.register)
router.put('/update', jwtHelper.requireJWTAuth, memberController.update)
router.put('/update-profile', jwtHelper.requireJWTAuth, memberController.updateProfile)
router.put('/update-location', jwtHelper.requireJWTAuth, memberController.updateLocation)
router.put('/change-password', jwtHelper.requireJWTAuth, memberController.changePassword)
router.put('/change-password-member', jwtHelper.requireJWTAuth, memberController.changePasswordMember)
router.put('/forget-password', memberController.forgetPassword)
router.delete('/:id', jwtHelper.requireJWTAuth, memberController.delete)

module.exports = router