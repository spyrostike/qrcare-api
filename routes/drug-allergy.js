const express = require('express')
const drugAllergyController = require('../controller/drugAllergyController')
const jwtHelper = require('../services/jwtHelper')

const router = express.Router()

router.post('/add', jwtHelper.requireJWTAuth, drugAllergyController.add)
router.put('/update', jwtHelper.requireJWTAuth, drugAllergyController.update)
router.get('/list/:creature_id', drugAllergyController.getDrugAllergyListByCreatureId)
router.get('/get-drug-allergy-list', jwtHelper.requireJWTAuth, drugAllergyController.getDrugAllergyList)
router.get('/:id', drugAllergyController.getDrugAllergyById)
router.get('/count/:creature_id', drugAllergyController.countDrugAllergyByCreatureId)
router.delete('/delete/:id', jwtHelper.requireJWTAuth, drugAllergyController.delete)
router.put('/delete', jwtHelper.requireJWTAuth, drugAllergyController.deleteItems)

module.exports = router