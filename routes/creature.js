const express = require('express')
const creatureController = require('../controller/creatureController')
const jwtHelper = require('../services/jwtHelper')

const router = express.Router()

router.post('/add', jwtHelper.requireJWTAuth, creatureController.add)
router.put('/update', jwtHelper.requireJWTAuth, creatureController.update)
router.get('/list', jwtHelper.requireJWTAuth, creatureController.getCreatureListByMemberId)
router.get('/get-creature-list', jwtHelper.requireJWTAuth, creatureController.getCreatureList)
router.get('/creature-by-qrcode-id/:qrcode_id', creatureController.getCreatureByQRCodeId)
router.get('/:id', creatureController.getCreatureById)
router.delete('/delete/:id', jwtHelper.requireJWTAuth, creatureController.delete)
router.put('/delete', jwtHelper.requireJWTAuth, creatureController.deleteItems)

module.exports = router