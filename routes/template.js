const express = require('express')
const router = express.Router()

const templateController = require('../controller/templateController')

router.get('/pattern/qrcode-set-pattern', templateController.qrcodeSetPattern)

module.exports = router