const express = require('express')
const treatmentHistoryController = require('../controller/treatmentHistoryController')
const jwtHelper = require('../services/jwtHelper')

const router = express.Router()

router.post('/add', jwtHelper.requireJWTAuth, treatmentHistoryController.add)
router.put('/update', jwtHelper.requireJWTAuth, treatmentHistoryController.update)
router.get('/get-treatment-history-list', treatmentHistoryController.getTreatmentHistoryList)
router.get('/list/:creature_id', treatmentHistoryController.getTreatmentHistoryListByCreatureId)
router.get('/:id', treatmentHistoryController.getTreatmentHistoryById)
router.delete('/delete/:id', jwtHelper.requireJWTAuth, treatmentHistoryController.delete)
router.put('/delete', jwtHelper.requireJWTAuth, treatmentHistoryController.deleteItems)
router.post('/send-mail', treatmentHistoryController.sendMail)

module.exports = router