const express = require('express')
const router = express.Router()
const qrcodeController = require('../controller/qrcodeController')

const jwtHelper = require('../services/jwtHelper')

router.post('/generate', jwtHelper.requireJWTAuth, qrcodeController.generate)
router.get('/get-qrcode-set-list', jwtHelper.requireJWTAuth, qrcodeController.getQRCodeSetList)
router.get('/get-qrcode-set-by-id/:id', jwtHelper.requireJWTAuth, qrcodeController.getQRCodeSetById)
router.get('/generate-pdf/:size/:id/:ids', qrcodeController.genetatePDF)
router.put('/update-qrcode-set', jwtHelper.requireJWTAuth, qrcodeController.updateQRCodeSet)
router.get('/get-qrcode-item-list', jwtHelper.requireJWTAuth, qrcodeController.getQRCodeItemList)
router.get('/get-qrcode-item-by-id/:id', qrcodeController.getQRCodeItemById)
router.get('/get-qrcode-info', jwtHelper.requireJWTAuth, qrcodeController.getQRCodeInfo)

module.exports = router