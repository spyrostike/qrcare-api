const express = require('express')
const jwtHelper = require('../services/jwtHelper')
const vaccineController = require('../controller/vaccineController')

const router = express.Router()

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////// vaccine book ////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

router.post('/book/add', jwtHelper.requireJWTAuth, vaccineController.addBook)
router.put('/book/update', jwtHelper.requireJWTAuth, vaccineController.updateBook)
router.get('/book/get-vaccine-book-list', jwtHelper.requireJWTAuth, vaccineController.getVaccineBookList)
router.get('/book/list/:creature_id', vaccineController.getVaccineBookListByCreatureId)
router.get('/book/:id', vaccineController.getVaccineBookById)
router.delete('/book/:id', jwtHelper.requireJWTAuth, vaccineController.deleteBook)
router.put('/book', jwtHelper.requireJWTAuth, vaccineController.deleteBookItems)

///////////////////////////////////////////////////////////////////////////////
/////////////////////////////////// vaccine ///////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

router.post('/add', jwtHelper.requireJWTAuth, vaccineController.add)
router.put('/update', jwtHelper.requireJWTAuth, vaccineController.update)
router.get('/list/:vaccine_book_id', vaccineController.getVaccineListByBookId)
router.get('/list', vaccineController.getVaccineList)
router.get('/:id', vaccineController.getVaccineById)
router.delete('/:id', jwtHelper.requireJWTAuth, vaccineController.delete)

module.exports = router