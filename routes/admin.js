const express = require('express')
const router = express.Router()
const adminController = require('../controller/adminController')
const jwtHelper = require('../services/jwtHelper')

router.post('/authen', adminController.authen)
router.get('/profile', jwtHelper.requireJWTAuth, adminController.getProfile)
router.put('/update', jwtHelper.requireJWTAuth, adminController.update)
router.put('/change-password', jwtHelper.requireJWTAuth, adminController.changePassword)

module.exports = router