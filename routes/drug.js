const express = require('express')
const drugController = require('../controller/drugController')
const jwtHelper = require('../services/jwtHelper')

const router = express.Router()

router.post('/add', jwtHelper.requireJWTAuth, drugController.add)
router.put('/update', jwtHelper.requireJWTAuth, drugController.update)
router.get('/list/:creature_id', drugController.getDrugListByCreatureId)
router.get('/get-drug-list', jwtHelper.requireJWTAuth, drugController.getDrugList)
router.get('/:id', drugController.getDrugById)
router.get('/count/:creature_id', drugController.countDrugByCreatureId)
router.delete('/delete/:id', jwtHelper.requireJWTAuth, drugController.delete)
router.put('/delete', jwtHelper.requireJWTAuth, drugController.deleteItems)

module.exports = router