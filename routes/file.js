const express = require('express')
const path = require('path')
const dayjs = require('dayjs')
const fileController = require('../controller/fileController')
const dirHelper = require('../utils/dirHelper')
const jwtHelper = require('../services/jwtHelper')

const router = express.Router()
const multer = require('multer')

const upload = multer({ 
    storage:multer.diskStorage({
        destination: (req, file, callback) => {
            const dir = path.join(__dirname, '..', process.env.UPLOAD_PATH + dayjs().format('YYYYMMDDHH'))
            dirHelper.checkAndMkDir(dir, () => {
                callback(null, dir)
            })
        },
        filename: function (req, file, callback) {
            let mimetype = '.jpg'
            // if (file.mimetype === 'image/jpeg')
            let prefix = ''
            if (file.originalname === 'member-profile.js') prefix = 'MP'
            else if (file.originalname === 'member-location.js') prefix = 'ML'
            else if (file.originalname === 'creature-profile.js') prefix = 'CP'
            else if (file.originalname === 'creature-birth-certificate.js') prefix = 'CB'
            else if (file.originalname === 'drug.js') prefix = 'DG'
            else if (file.originalname === 'drug-allergy.js') prefix = 'DA'
            else if (file.originalname === 'food-allergy.js') prefix = 'FA'
            else if (file.originalname === 'treatment-history.js') prefix = 'TH'
            else if (file.originalname === 'vaccine.js') prefix = 'VC'
            else if (file.originalname === 'qrcode-set-background.js') prefix = 'QL'

            callback(null, prefix + new Date().getTime() + mimetype)
        }
    }),
    fileFilter: (req, file, callback) => {
        if (!file.mimetype.match(/\/(jpg|jpeg|png|gif)$/)) { 
            return callback(new Error('File is invalid.'), false)
        }

        callback(null, true)
    },
    limits: {
        fileSize: process.env.FILE_SIZE_LIMIT 
    }
})

router.post('/add', upload.single('file-data'), fileController.add)
router.put('/set-files-ref-id', jwtHelper.requireJWTAuth, fileController.setFilesRefId)
router.get('/get-file-list', fileController.getFileList)
router.delete('/:id', jwtHelper.requireJWTAuth, fileController.delete)
router.put('/delete', jwtHelper.requireJWTAuth, fileController.deleteItems)

module.exports = router