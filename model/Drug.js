const dayjs = require('dayjs')
const moment = require('moment')
const ObjectUtil = require('../utils/ObjectUtil')

module.exports = {
    add: async (con, params, user) => {
        let rowNo = await con('drug').select('number').orderBy('number', 'desc').first()
        let number = 0
        if (rowNo) number = rowNo.number
        
        const drug = ObjectUtil.generateDrugForInsertToDb(params, user, number)
   
        return await con.transaction((trx)  => {
            con('drug')
            .transacting(trx)
            .returning('id')
            .insert(drug)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    update: async (con, params, user) => {
        const { id } = params

        const drug = ObjectUtil.generateDrugForUpdateToDb(params, user)
        console.log('drug', drug)
        return await con.transaction(trx => {
            con('drug')
            .update(drug)
            .where('id', id)
            .returning('id')
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    getDrugById: async (con, params) => {
        const { id, status } = params 
        const iStatus = status || 1
        
        return await con('drug')
            .select('*')
            .where('status', iStatus)
            .andWhere('id', id)
            .first()
    },
    getDrugList: async (con, params) => {
        const { 
            creatureId, 
            nameCriteria, 
            createDateCriteria, 
            limit, 
            offset, 
            orderByKey, 
            orderByValue, 
            status } = params

        const iOrderByKey = orderByKey || 'number'
        const iOrderByValue = orderByValue || 'asc'
        const iNameCriteria = nameCriteria || ''
        const iCreateDateCriteria = createDateCriteria || ''
        const iOffset = offset || 0
        const iLimit = limit || 10
        const iStatus = status || 1

        let whereFirst = iStatus !== null ? 'd.status = :status ' : ''
        
        if (iNameCriteria && iNameCriteria.trim().length > 0) {
            whereFirst += ' and d.name like :name '
        }

        if (moment(iCreateDateCriteria, 'YYYY/MM/DD').format('YYYY/MM/DD') === iCreateDateCriteria) {
            whereFirst += ' and cast (d.create_date as date) = :create_date '
        }

        return await con({ d: 'drug' })
            .select(
                {
                    id: 'd.id',
                    code: 'd.code',
                    number: 'd.number',
                    name: 'd.name',
                    description: 'd.description',
                    member_id: 'd.member_id',
                    creature_id: 'd.creature_id',
                    status: 'd.status',
                    create_by: 'd.create_by',
                    create_date: 'd.create_date',
                    edit_by: 'd.edit_by',
                    edit_date: 'd.edit_date'
                }
            )
            .whereRaw(
                whereFirst, 
                {
                    status: iStatus,
                    name: iNameCriteria + '%',
                    create_date: iCreateDateCriteria
                }
            )
            .andWhere('creature_id', creatureId)
            .orderBy('d.' + iOrderByKey, iOrderByValue)
            .limit(iLimit)
            .offset(iOffset * iLimit)
    },
    getDrugCount: async (con, params) => {
        const { creatureId, nameCriteria, createDateCriteria, status } = params

        const iNameCriteria = nameCriteria || ''
        const iCreateDateCriteria = createDateCriteria || ''
        const iStatus = status || 1

        let whereFirst = iStatus !== null ? 'd.status = :status ' : ''
        
        if (iNameCriteria && iNameCriteria.trim().length > 0) {
            whereFirst += ' and d.name like :name '
        }

        if (moment(iCreateDateCriteria, 'YYYY/MM/DD').format('YYYY/MM/DD') === iCreateDateCriteria) {
            whereFirst += ' and cast (d.create_date as date) = :create_date '
        }

        return await con({ d: 'drug' })
            .count('*')
            .whereRaw(
                whereFirst, 
                {
                    status: iStatus,
                    name: iNameCriteria + '%',
                    create_date: iCreateDateCriteria
                }
            )
            .andWhere('d.creature_id', creatureId)
    },
    getDrugListByCreatureId: async (con, params) => {
        const { creature_id, status } = params 
        const iStatus = status || 1

        return await con({ d: 'drug' })
            .select({
                id: 'd.id',
                code: 'd.code',
                number: 'd.number',
                name: 'd.name',
                description: 'd.description',
                member_id: 'd.member_id',
                creature_id: 'd.creature_id',
                status: 'd.status',
                create_by: 'd.create_by',
                create_date: 'd.create_date',
                edit_by: 'd.edit_by',
                edit_date: 'd.edit_date'
            })
            .where('d.status', iStatus)
            .andWhere('d.creature_id', creature_id)
            .orderBy('d.number', 'asc')
    }, 
    countDrugByCreatureId: async (con, params) => {
        const { creature_id, status } = params 
        const iStatus = status || 1
        
        return await con('drug')
            .count('*')
            .where('status', iStatus)
            .andWhere('creature_id', creature_id)
    },
    delete: async (con, params, user) => {
        return await con('drug')
            .update({
                status: 2,
                edit_by: user.id,
                edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
            })
            .where('id', params.id)
    },
    deleteItems: async (con, params, user) => {
        return await con('drug')
            .update({
                status: 2,
                edit_by: user.id,
                edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
            })
            .whereIn('id', params.id)
    }
}