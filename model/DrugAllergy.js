const dayjs = require('dayjs')
const moment = require('moment')
const ObjectUtil = require('../utils/ObjectUtil')

module.exports = {
    add: async (con, params, user) => {
        let rowNo = await con('drug_allergy').select('number').orderBy('number', 'desc').first()
        let number = 0
        if (rowNo) number = rowNo.number
        
        const drugAllergy = ObjectUtil.generateDrugAllergyForInsertToDb(params, user, number)

        return await con.transaction((trx)  => {
            con('drug_allergy')
            .transacting(trx)
            .returning('id')
            .insert(drugAllergy)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    update: async (con, params, user) => {
        const { id } = params

        const drugAllergy = ObjectUtil.generateDrugAllergyForUpdateToDb(params, user)

        return await con.transaction(trx => {
            con('drug_allergy')
            .update(drugAllergy)
            .returning('id')
            .where('id', id)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    getDrugAllergyById: async (con, params) => {
        const { id, status } = params 
        const iStatus = status || 1
        
        return await con('drug_allergy')
            .select('*')
            .where('status', iStatus)
            .andWhere('id', id)
            .first()
    },
    getDrugAllergyList: async (con, params) => {
        const { 
            creatureId, 
            nameCriteria, 
            createDateCriteria, 
            limit, 
            offset, 
            orderByKey, 
            orderByValue, 
            status } = params

        const iOrderByKey = orderByKey || 'number'
        const iOrderByValue = orderByValue || 'asc'
        const iNameCriteria = nameCriteria || ''
        const iCreateDateCriteria = createDateCriteria || ''
        const iOffset = offset || 0
        const iLimit = limit || 10
        const iStatus = status || 1

        let whereFirst = iStatus !== null ? 'da.status = :status ' : ''

        if (iNameCriteria && iNameCriteria.trim().length > 0) {
            whereFirst += ' and da.name like :name '
        }

        if (moment(iCreateDateCriteria, 'YYYY/MM/DD').format('YYYY/MM/DD') === iCreateDateCriteria) {
            whereFirst += ' and cast (da.create_date as date) = :create_date '
        }

        return await con({ da: 'drug_allergy' })
            .select(
                {
                    id: 'da.id',
                    code: 'da.code',
                    number: 'da.number',
                    name: 'da.name',
                    description: 'da.description',
                    creature_id: 'da.creature_id',
                    description: 'da.description',
                    member_id: 'da.member_id',
                    status: 'da.status',
                    create_date: 'da.create_date',
                    create_by: 'da.create_by',
                    edit_date: 'da.edit_date',
                    edit_by: 'da.edit_by'
                }
            )
            .whereRaw(
                whereFirst, 
                {
                    status: iStatus,
                    name: iNameCriteria + '%',
                    create_date: iCreateDateCriteria
                }
            )
            .andWhere('creature_id', creatureId)
            .orderBy('da.' + iOrderByKey, iOrderByValue)
            .limit(iLimit)
            .offset(iOffset * iLimit)
    },
    getDrugAllergyCount: async (con, params) => {
        const { creatureId, nameCriteria, createDateCriteria, status } = params

        const iNameCriteria = nameCriteria || ''
        const iCreateDateCriteria = createDateCriteria || ''
        const iStatus = status || 1

        let whereFirst = iStatus !== null ? 'da.status = :status ' : ''

        if (iNameCriteria && iNameCriteria.trim().length > 0) {
            whereFirst += ' and da.name like :name '
        }

        if (moment(iCreateDateCriteria, 'YYYY/MM/DD').format('YYYY/MM/DD') === iCreateDateCriteria) {
            whereFirst += ' and cast (da.create_date as date) = :create_date '
        }

        return await con({ da: 'drug_allergy' })
            .count('*')
            .whereRaw(
                whereFirst, 
                {
                    status: iStatus,
                    name: iNameCriteria + '%',
                    create_date: iCreateDateCriteria
                }
            )
            .andWhere('da.creature_id', creatureId)
    },
    getDrugAllergyListByCreatureId: async (con, params) => {
        const { creature_id, status } = params 
        const iStatus = status || 1

        return await con({ da: 'drug_allergy' })
            .select({
                id: 'da.id',
                code: 'da.code',
                number: 'da.number',
                name: 'da.name',
                description: 'da.description',
                creature_id: 'da.creature_id',
                description: 'da.description',
                member_id: 'da.member_id',
                status: 'da.status',
                create_date: 'da.create_date',
                create_by: 'da.create_by',
                edit_date: 'da.edit_date',
                edit_by: 'da.edit_by'
            })
            .where('da.status', iStatus)
            .andWhere('da.creature_id', creature_id)
            .orderBy('da.number', 'asc')
    },
    countDrugAllergyByCreatureId: async (con, params) => {
        const { creature_id, status } = params 
        const iStatus = status || 1
        
        return await con('drug_allergy')
            .count('*')
            .where('status', iStatus)
            .andWhere('creature_id', creature_id)
    },
    delete: async (con, params, user) => {
        return await con('drug_allergy')
            .update({
                status: 2,
                edit_by: user.id,
                edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
            })
            .where('id', params.id)
    },
    deleteItems: async (con, params, user) => {
        return await con('drug_allergy')
            .update({
                status: 2,
                edit_by: user.id,
                edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
            })
            .whereIn('id', params.id)
    }
}