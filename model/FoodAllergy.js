const dayjs = require('dayjs')
const moment = require('moment')
const ObjectUtil = require('../utils/ObjectUtil')

module.exports = {
    add: async (con, params, user) => {
        let rowNo = await con('food_allergy').select('number').orderBy('number', 'desc').first()
        let number = 0
        if (rowNo) number = rowNo.number
        
        const foodAllergy = ObjectUtil.generateFoodAllergyForInsertToDb(params, user, number)

        return await con.transaction((trx)  => {
            con('food_allergy')
            .transacting(trx)
            .returning('id')
            .insert(foodAllergy)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    update: async (con, params, user) => {
        const { id } = params

        const foodAllergy = ObjectUtil.generateFoodAllergyForUpdateToDb(params, user)

        return await con.transaction(trx => {
            con('food_allergy')
            .update(foodAllergy)
            .returning('id')
            .where('id', id)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    getFoodAllergyList: async (con, params) => {
        const { 
            creatureId, 
            nameCriteria, 
            createDateCriteria, 
            limit, 
            offset, 
            orderByKey, 
            orderByValue, 
            status } = params

        const iOrderByKey = orderByKey || 'number'
        const iOrderByValue = orderByValue || 'asc'
        const iNameCriteria = nameCriteria || ''
        const iCreateDateCriteria = createDateCriteria || ''
        const iOffset = offset || 0
        const iLimit = limit || 10
        const iStatus = status || 1

        let whereFirst = iStatus !== null ? 'fa.status = :status ' : ''
        
        if (iNameCriteria && iNameCriteria.trim().length > 0) {
            whereFirst += ' and fa.name like :name '
        }   
        
        if (moment(iCreateDateCriteria, 'YYYY/MM/DD').format('YYYY/MM/DD') === iCreateDateCriteria) {
            whereFirst += ' and cast (fa.create_date as date) = :create_date '
        }

        return await con({ fa: 'food_allergy' })
            .select(
                {
                    id: 'fa.id',
                    code: 'fa.code',
                    number: 'fa.number',
                    name: 'fa.name',
                    description: 'fa.description',
                    creature_id: 'fa.creature_id',
                    description: 'fa.description',
                    member_id: 'fa.member_id',
                    status: 'fa.status',
                    create_date: 'fa.create_date',
                    create_by: 'fa.create_by',
                    edit_date: 'fa.edit_date',
                    edit_by: 'fa.edit_by'
                }
            )
            .whereRaw(
                whereFirst, 
                {
                    status: iStatus,
                    name: iNameCriteria + '%',
                    create_date: iCreateDateCriteria
                }
            )
            .andWhere('creature_id', creatureId)
            .orderBy('fa.' + iOrderByKey, iOrderByValue)
            .limit(iLimit)
            .offset(iOffset * iLimit)
    },
    getFoodAllergyCount: async (con, params) => {
        const { creatureId, nameCriteria, createDateCriteria, status } = params

        const iNameCriteria = nameCriteria || ''
        const iCreateDateCriteria = createDateCriteria || ''
        const iStatus = status || 1

        let whereFirst = iStatus !== null ? 'fa.status = :status ' : ''

        if (iNameCriteria && iNameCriteria.trim().length > 0) {
            whereFirst += ' and fa.name like :name '
        }

        if (moment(iCreateDateCriteria, 'YYYY/MM/DD').format('YYYY/MM/DD') === iCreateDateCriteria) {
            whereFirst += ' and cast (fa.create_date as date) = :create_date '
        }

        return await con({ fa: 'food_allergy' })
            .count('*')
            .whereRaw(
                whereFirst, 
                {
                    status: iStatus,
                    name: iNameCriteria + '%',
                    create_date: iCreateDateCriteria
                }
            )
            .andWhere('fa.creature_id', creatureId)
    },
    getFoodAllergyById: async (con, params) => {
        const { id, status } = params 
        const iStatus = status || 1
        
        return await con('food_allergy')
            .select('*')
            .where('status', iStatus)
            .andWhere('id', id)
            .first()
    },
    getFoodAllergyListByCreatureId: async (con, params) => {
        const { creature_id, status } = params 
        const iStatus = status || 1

        return await con({ fa: 'food_allergy' })
            .select({
                id: 'fa.id',
                code: 'fa.code',
                number: 'fa.number',
                name: 'fa.name',
                description: 'fa.description',
                creature_id: 'fa.creature_id',
                member_id: 'fa.member_id',
                status: 'fa.status',
                create_date: 'fa.create_date',
                create_by: 'fa.create_by',
                edit_date: 'fa.edit_date',
                edit_by: 'fa.edit_by'
            })
            .where('fa.status', iStatus)
            .andWhere('fa.creature_id', creature_id)
            .orderBy('fa.number', 'asc')
    },
    countFoodAllergyByCreatureId: async (con, params) => {
        const { creature_id, status } = params 
        const iStatus = status || 1
        
        return await con('food_allergy')
            .count('*')
            .where('status', iStatus)
            .andWhere('creature_id', creature_id)
    },
    delete: async (con, params, user) => {
        return await con('food_allergy')
            .update({
                status: 2,
                edit_by: user.id,
                edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
            })
            .where('id', params.id)
    },
    deleteItems: async (con, params, user) => {
        return await con('food_allergy')
            .update({
                status: 2,
                edit_by: user.id,
                edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
            })
            .whereIn('id', params.id)
    }
}