const ObjectUtil = require('../utils/ObjectUtil')
const dayjs = require('dayjs')
const moment = require('moment')

module.exports = {
    add: async (con, params, user) => {
        const { qrcodeItemId } = params

        let rowNo = await con('creature').select('number').orderBy('number', 'desc').first()
        let number = 0
        if (rowNo) number = rowNo.number

        const creature = ObjectUtil.generateCreatureForInsertToDb(params, user, number)

        return await con.transaction((trx)  => {
            con('qrcode_item')
            .transacting(trx)
            .returning('id')
            .update({  
                usage_status: 3,
                edit_by: user.id,
                edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
            }).where('id', qrcodeItemId)
            .then(id => {
                return con('creature')
                .insert(creature)
                .transacting(trx)
                .returning('id')
            })
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    update: async (con, params, user) => {
        const { id } = params

        const creature = ObjectUtil.generateCreatureForUpdateToDb(params, user)

        return await con.transaction(trx => {
            con('creature')
            .update(creature)
            .returning('id')
            .where('id', id)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    getCreatureById: async (con, params) => {
        const { id, status } = params 
        const iStatus = status || 1

        return await con({ ct: 'creature' })
            .select({
                id: 'ct.id',
                code: 'ct.code',
                number: 'ct.number',
                first_name: 'ct.first_name',
                last_name: 'ct.last_name',
                gender: 'ct.gender',
                blood_type: 'ct.blood_type',
                birth_date: 'ct.birth_date',
                weight: 'ct.weight',
                height: 'ct.height',
                member_id: 'ct.member_id',
                qrcode_set_id: 'ct.qrcode_set_id',
                qrcode_item_id: 'ct.qrcode_item_id',
                qrcode_item_code: 'qi.code',
                status: 'ct.status',
                create_date: 'ct.create_date',
                create_by: 'ct.create_by',
                edit_date: 'ct.edit_date',
                edit_by: 'ct.edit_by',
                gender_name: 'gd.name_eng',
                blood_type_name: 'bt.name_eng',
                member: con.raw('concat( mb.first_name, \' \', mb.last_name )'),
                creature_image_id: 'cp.id',
                creature_image_path: 'cp.path',
                creature_image_name: 'cp.name',
                birth_certificate_image_id: 'cb.id',
                birth_certificate_image_path: 'cb.path',
                birth_certificate_image_name: 'cb.name',
                status_name: 'st.name_eng'
            })
            .leftJoin('file as cp', function() { 
                this.on('ct.id', '=', 'cp.ref_id')
                this.andOnVal('cp.tag', '=', 'creature-profile-image')
                this.andOnVal('cp.status', '=', 1)
            })
            .leftJoin('file as cb', function() { 
                this.on('ct.id', '=', 'cb.ref_id')
                this.andOnVal('cb.tag', '=', 'creature-birth-certificate-image')
                this.andOnVal('cb.status', '=', 1)
            })
            .innerJoin('member as mb', 'mb.id', 'ct.member_id')
            .innerJoin('gender as gd', 'ct.gender', 'gd.id')
            .innerJoin('blood_type as bt', 'ct.blood_type', 'bt.id')
            .innerJoin('status as st', 'ct.status', 'st.id')
            .innerJoin('qrcode_item as qi', 'ct.qrcode_item_id', 'qi.id')
            .where('ct.status', iStatus)
            .andWhere('ct.id', id)
            .first()
    },
    getCreatureByQRCodeId: async (con, params) => {
        const { qrcode_id, status } = params 
        const iStatus = status || 1

        return await con({ ct: 'creature' })
            .select({
                id: 'ct.id',
                code: 'ct.code',
                number: 'ct.number',
                first_name: 'ct.first_name',
                last_name: 'ct.last_name',
                gender: 'ct.gender',
                blood_type: 'ct.blood_type',
                birth_date: 'ct.birth_date',
                weight: 'ct.weight',
                height: 'ct.height',
                member_id: 'ct.member_id',
                qrcode_set_id: 'ct.qrcode_set_id',
                qrcode_item_id: 'ct.qrcode_item_id',
                status: 'ct.status',
                create_date: 'ct.create_date',
                create_by: 'ct.create_by',
                edit_date: 'ct.edit_date',
                edit_by: 'ct.edit_by',
                gender_name: 'gd.name_eng',
                blood_type_name: 'bt.name_eng'
            })
            .innerJoin('gender as gd', 'ct.gender', 'gd.id')
            .innerJoin('blood_type as bt', 'ct.blood_type', 'bt.id')
            .where('ct.status', iStatus)
            .andWhere('qrcode_item_id', qrcode_id)
            .first()
    },
    getCreatureListByMemberId: async (con, params, user) => {
        const { status } = params 
        const iStatus = status || 1

        return await con({ ct: 'creature' })
            .select({
                id: 'ct.id',
                code: 'ct.code',
                number: 'ct.number',
                first_name: 'ct.first_name',
                last_name: 'ct.last_name',
                gender: 'ct.gender',
                blood_type: 'ct.blood_type',
                birth_date: 'ct.birth_date',
                weight: 'ct.weight',
                height: 'ct.height',
                member_id: 'ct.member_id',
                qrcode_set_id: 'ct.qrcode_set_id',
                qrcode_item_id: 'ct.qrcode_item_id',
                status: 'ct.status',
                create_date: 'ct.create_date',
                create_by: 'ct.create_by',
                edit_date: 'ct.edit_date',
                edit_by: 'ct.edit_by',
                gender_name: 'gd.name_eng',
                blood_type_name: 'bt.name_eng',
            })
            .innerJoin('gender as gd', 'ct.gender', 'gd.id')
            .innerJoin('blood_type as bt', 'ct.blood_type', 'bt.id')
            .where('ct.status', iStatus)
            .andWhere('ct.member_id', user.id)
            .orderBy('number', 'asc')
        
    },
    getCreatureList: async (con, params) => {
        const { 
            limit, 
            offset, 
            orderByKey, 
            orderByValue, 
            status, 
            serialNoCriteria, 
            nameCriteria, 
            birthDateCriteria } = params

        const iOrderByKey = orderByKey || 'number'
        const iOrderByValue = orderByValue || 'asc'
        const iSerialNoCriteria = serialNoCriteria || ''
        const iNameCriteria = nameCriteria || ''
        const iBirthDateCriteria = birthDateCriteria || ''
        const iOffset = offset || 0
        const iLimit = limit || 10
        const iStatus = status || 1

        let whereFirst = iStatus !== null ? 'ct.status = :status ' : ''

        if (iSerialNoCriteria && iSerialNoCriteria.trim().length > 0) {
            whereFirst += ' and ct.code like :code '
        }

        if (iNameCriteria && iNameCriteria.trim().length > 0) {
            whereFirst += ' and CONCAT(ct.first_name, \' \', ct.last_name) like :name '
        }

        if (moment(iBirthDateCriteria, 'YYYY/MM/DD').format('YYYY/MM/DD') === iBirthDateCriteria) {
            whereFirst += ' and cast (ct.birth_date as date) = :birth_date '
        }

        return await con({ ct: 'creature'})
            .select(
                {
                    id: 'ct.id',
                    code: 'ct.code',
                    number: 'ct.number',
                    name: con.raw('concat( ct.first_name, \' \', ct.last_name )'),
                    first_name: 'ct.first_name',
                    last_name: 'ct.last_name',
                    gender: 'ct.gender',
                    blood_type: 'ct.blood_type',
                    birth_date: 'ct.birth_date',
                    weight: 'ct.weight',
                    height: 'ct.height',
                    member_id: 'ct.member_id',
                    qrcode_set_id: 'ct.qrcode_set_id',
                    qrcode_item_id: 'ct.qrcode_item_id',
                    status: 'ct.status',
                    create_date: 'ct.create_date',
                    create_by: 'ct.create_by',
                    edit_date: 'ct.edit_date',
                    create_date: 'ct.create_date',
                    gender_name: 'gd.name_eng',
                    blood_type_name: 'bt.name_eng',
                    member: con.raw('concat( mb.first_name, \' \', mb.last_name )')
                }
            )
            .innerJoin('gender as gd', 'ct.gender', 'gd.id')
            .innerJoin('blood_type as bt', 'ct.blood_type', 'bt.id')
            .innerJoin('member as mb', 'mb.id', 'ct.member_id')
            .whereRaw(
                whereFirst,
                {
                    status: iStatus,
                    code: iSerialNoCriteria + '%',
                    name: iNameCriteria + '%',
                    birth_date: iBirthDateCriteria
                }
            )
            .orderBy('ct.' + iOrderByKey, iOrderByValue)
            .limit(iLimit)
            .offset(iOffset * iLimit)
    },
    getCreatureCount: async (con, params) => {
        const { serialNoCriteria, nameCriteria, birthDateCriteria, status } = params

        const iSerialNoCriteria = serialNoCriteria || ''
        const iNameCriteria = nameCriteria || ''
        const iBirthDateCriteria = birthDateCriteria || ''
        const iStatus = status || 1

        let whereFirst = iStatus !== null ? 'ct.status = :status ' : ''

        if (iSerialNoCriteria && iSerialNoCriteria.trim().length > 0) {
            whereFirst += ' and ct.code like :code '
        }

        if (iNameCriteria && iNameCriteria.trim().length > 0) {
            whereFirst += ' and CONCAT(ct.first_name, \' \', ct.last_name) like :name '
        }

        if (moment(iBirthDateCriteria, 'YYYY/MM/DD').format('YYYY/MM/DD') === iBirthDateCriteria) {
            whereFirst += ' and cast (ct.birth_date as date) = :birth_date '
        }

        return await con({ ct: 'creature' })
            .count('*')
            .whereRaw(
                whereFirst, 
                {
                    status: iStatus,
                    code: iSerialNoCriteria + '%',
                    name: iNameCriteria + '%',
                    birth_date: iBirthDateCriteria
                }
            )
    }, 
    delete: async (con, params, user) => {
        return await con('creature')
            .update({
                status: 2,
                edit_by: user.id,
                edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
            })
            .where('id', params.id)
    }
}