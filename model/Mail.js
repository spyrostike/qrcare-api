const dayjs = require('dayjs')
const ObjectUtil = require('../utils/ObjectUtil')

module.exports = {
    add: async (con, params, user) => {

        const mail = ObjectUtil.generateMailForInsertToDb(params, user)
   
        return await con.transaction((trx)  => {
            con('mail')
            .transacting(trx)
            .returning('id')
            .insert(mail)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    getMailListUnsendSuccess: async (con) => {
        return await con('mail')
            .select('*')
            .whereNot('mail_status', 2)
            .where('status', 1)
    },
    updateMailItemsSuccess: async (con, params) => {
        return await con('mail')
            .update({
                mail_status: 2,
                sent_time: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
            })
            .whereIn('id', params.items)
    },
    updateMailFailed: async (con, params) => {
        return await con('mail')
            .update({
                mail_status: 3,
                error_message: params.error_message,
                sent_time: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
            })
            .where('id', params.id)
    }
}