const dayjs = require('dayjs')
const moment = require('moment')
const ObjectUtil = require('../utils/ObjectUtil')

module.exports = {
    authenticate: async (con, params) => {
        return await con.select('*').from('member').where('id', params.id).andWhere('status', 1).first()
    },
    checkUserNameExist: async (con, params) => {
        return await con.select('*').from('member').where('username', params.username).first()
    },
    checkEmailExist: async (con, params) => {
        return await con.select('*').from('member').where('email', params.email).first()
    },
    register: async (con, params) => {
        const { qrcodeItemId } = params
        
        let rowNo = await con('member').select('number').orderBy('number', 'desc').first()
        let number = 0
        if (rowNo) number = rowNo.number

        const member = await ObjectUtil.generateMemberForInsertToDb(params, number)

        return await con.transaction((trx)  => {
            con('member')
            .transacting(trx)
            .returning('id')
            .insert(member)
            .then(memberId => {
                return con('qrcode_item')
                .update({  
                    usage_status: 2,
                    edit_by: memberId[0],
                    edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
                }).where('id', qrcodeItemId)
                .transacting(trx)
                .returning('edit_by')
            })
            .then(trx.commit)
            .catch(trx.rollback)

        })
    },
    update: async (con, params, user) => {
        const { id } = params

        const member = ObjectUtil.generateMemberForUpdateToDb(params, user)

        return await con.transaction(trx => {
            con('member')
            .update(member)
            .where('id', id)
            .returning('id')
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    updateProfile: async (con, params, user) => {

        const member = ObjectUtil.generateMemberProfileForUpdateToDb(params, user)

        return await con.transaction(trx => {
            con('member')
            .update(member)
            .where('id', user.id)
            .returning('id')
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    updateLocation: async (con, params, user) => {

        const member = ObjectUtil.generateMemberLocationForUpdateToDb(params, user)

        return await con.transaction(trx => {
            con('member')
            .update(member)
            .where('id', user.id)
            .returning('id')
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    changePassword: async (con, params, user) => {
        const member = await ObjectUtil.generateMemberChangePasswordForUpdateToDb(params, user)

        return await con.transaction(trx => {
            con('member')
            .update(member)
            .where('id', user.id)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    changePasswordMember: async (con, params, user) => {
        const { id } = params
        const member = await ObjectUtil.generateMemberChangePasswordForUpdateToDb(params, user)

        return await con.transaction(trx => {
            con('member')
            .update(member)
            .where('id', id)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    getMemberByQRCodeItemId: async (con, params) => {
        const { qrcodeItemId, status } = params

        const iStatus = status || 1

        return await con({ mb: 'member' })
            .select({
                id: 'mb.id',
                code: 'mb.code',
                number: 'mb.number',
                first_name: 'first_name',
                last_name: 'last_name',
                gender: 'gender',
                blood_type: 'blood_type',
                birth_date: 'birth_date',
                relationship: 'relationship',
                identification_no: 'identification_no',
                contact_no1: 'contact_no1',
                contact_no2: 'contact_no2',
                address1: 'address1',
                address2: 'address2',
                address3: 'address3',
                profile_image_id: 'profile_image_id',
                place_image_id: 'place_image_id',
                place_latitude: 'place_latitude',
                place_longitude: 'place_longitude',
                qrcode_item_id: 'qrcode_item_id',
                qrcode_item_id: 'qrcode_item_id',
                status: 'status',
                create_date: 'create_date',
                create_by: 'create_by',
                edit_date: 'edit_date',
                edit_by: 'edit_by'

            })
            .where('mb.status', iStatus)
            .andWhere('mb.qrcode_item_id', qrcodeItemId)
            .first()
    },
    getMemberList: async (con, params) => {
        const { 
            limit, 
            offset, 
            orderByKey, 
            orderByValue, 
            status, 
            textSearch,
            usernameCriteria, 
            nameCriteria, 
            birthDateCriteria } = params

        const iOrderByKey = orderByKey || 'number'
        const iOrderByValue = orderByValue || 'asc'
        let iTextSearch = textSearch || ''
        // const iDate = date ||  dayjs().format('YYYY-MM-D')
        const iUsernameCriteria= usernameCriteria || ''
        const iNameCriteria = nameCriteria || ''
        const iBirthDateCriteria = birthDateCriteria || ''
        const iOffset = offset || 0
        const iLimit = limit || 10
        const iStatus = status || 1

        let whereFirst = iStatus !== null ? 'mb.status = :status ' : ''

        if (iUsernameCriteria && iUsernameCriteria.trim().length > 0) {
            whereFirst += 'and mb.username like :username '
        }

        if (iNameCriteria && iNameCriteria.trim().length > 0) {
            whereFirst += ' and CONCAT(mb.first_name, \' \', mb.last_name) like :name '
        }

        if (moment(iBirthDateCriteria, 'YYYY/MM/DD').format('YYYY/MM/DD') === iBirthDateCriteria) {
            whereFirst += ' and cast (mb.birth_date as date) = :birth_date '
        }

        return await con({ mb: 'member'})
            .select(
                {
                    id: 'mb.id',
                    code: 'mb.code',
                    number: 'mb.number',
                    username: 'mb.username',
                    name: con.raw('concat( mb.first_name, \' \', mb.last_name )'),
                    first_name: 'mb.first_name',
                    last_name: 'mb.last_name',
                    gender: 'mb.gender',
                    blood_type: 'mb.blood_type',
                    birth_date: 'mb.birth_date',
                    relationship: 'mb.relationship',
                    identification_no: 'mb.identification_no',
                    contact_no1: 'mb.contact_no1',
                    contact_no2: 'mb.contact_no2',
                    address1: 'mb.address1',
                    address2: 'mb.address2',
                    address3: 'mb.address3',
                    status: 'mb.status',
                    status_name: 'st.name_eng',
                    create_date: 'mb.create_date',
                    create_by: con.raw('( select concat( ad.first_name, \' \', ad.last_name ) from admin ad where  ad.id = mb.create_by )')
                }
            )
            .whereRaw(
                whereFirst,
                {
                    status: iStatus,
                    username: iUsernameCriteria + '%',
                    name: iNameCriteria + '%',
                    birth_date: iBirthDateCriteria
                }
            )
            .innerJoin('status as st', 'mb.status', 'st.id')
            .orderBy('mb.' + iOrderByKey, iOrderByValue)
            .limit(iLimit)
            .offset(iOffset * iLimit)
    },
    getMemberCount: async (con, params) => {
        const {usernameCriteria, nameCriteria, birthDateCriteria, status } = params

        const iUsernameCriteria= usernameCriteria || ''
        const iNameCriteria = nameCriteria || ''
        const iBirthDateCriteria = birthDateCriteria || ''
        const iStatus = status || 1

        let whereFirst = iStatus !== null ? 'mb.status = :status ' : ''

        if (iUsernameCriteria && iUsernameCriteria.trim().length > 0) {
            whereFirst += 'and mb.username like :username '
        }

        if (iNameCriteria && iNameCriteria.trim().length > 0) {
            whereFirst += ' and CONCAT(mb.first_name, \' \', mb.last_name) like :name '
        }

        if (moment(iBirthDateCriteria, 'YYYY/MM/DD').format('YYYY/MM/DD') === iBirthDateCriteria) {
            whereFirst += ' and cast (mb.birth_date as date) = :birth_date '
        }

        return await con({mb: 'member' })
            .count('*')
            .whereRaw(
                whereFirst, 
                {
                    status: iStatus,
                    username: iUsernameCriteria + '%',
                    name: iNameCriteria + '%',
                    birth_date: iBirthDateCriteria
                }
            )
    },
    getMemberByIdAllProps: async (con, params) => {
        const { id, status } = params
        
        const iStatus = status || 1

        return await con({ mb: 'member' })
            .select({
                id: 'mb.id',
                code: 'mb.code',
                number: 'mb.number',
                first_name: 'mb.first_name',
                last_name: 'mb.last_name',
                gender: 'mb.gender',
                blood_type: 'mb.blood_type',
                birth_date: 'mb.birth_date',
                relationship: 'mb.relationship',
                identification_no: 'mb.identification_no',
                contact_no1: 'mb.contact_no1',
                contact_no2: 'mb.contact_no2',
                address1: 'mb.address1',
                address2: 'mb.address2',
                address3: 'mb.address3',
                username: 'mb.username',
                password: 'mb.password',
                private_key: 'mb.private_key',
                place_latitude: 'mb.place_latitude',
                place_longitude: 'mb.place_longitude',
                status: 'mb.status',
                create_date: 'mb.create_date',
                create_by: con.raw('( select concat( ad.first_name, \' \', ad.last_name ) from admin ad where  ad.id = mb.create_by )'),
                create_date: 'mb.create_date',
                edit_by: con.raw('( select concat( ad.first_name, \' \', ad.last_name ) from admin ad where  ad.id = mb.edit_by )'),
                edit_date: 'mb.edit_date',
                gender_name: 'gd.name_eng',
                blood_type_name: 'bt.name_eng'
            })
            .innerJoin('gender as gd', 'mb.gender', 'gd.id')
            .innerJoin('blood_type as bt', 'mb.blood_type', 'bt.id')
            .where('mb.status', iStatus)
            .andWhere('mb.id', id)
            .first()
    },
    getMemberByEmailAllProps: async (con, params) => {
        const { email, status } = params
        
        const iStatus = status || 1

        return await con({ mb: 'member' })
            .select({
                id: 'mb.id',
                code: 'mb.code',
                number: 'mb.number',
                first_name: 'mb.first_name',
                last_name: 'mb.last_name',
                gender: 'mb.gender',
                blood_type: 'mb.blood_type',
                birth_date: 'mb.birth_date',
                relationship: 'mb.relationship',
                identification_no: 'mb.identification_no',
                contact_no1: 'mb.contact_no1',
                contact_no2: 'mb.contact_no2',
                address1: 'mb.address1',
                address2: 'mb.address2',
                address3: 'mb.address3',
                username: 'mb.username',
                password: 'mb.password',
                private_key: 'mb.private_key',
                place_latitude: 'mb.place_latitude',
                place_longitude: 'mb.place_longitude',
                status: 'mb.status',
                create_date: 'mb.create_date',
                create_by: con.raw('( select concat( ad.first_name, \' \', ad.last_name ) from admin ad where  ad.id = mb.create_by )'),
                create_date: 'mb.create_date',
                edit_by: con.raw('( select concat( ad.first_name, \' \', ad.last_name ) from admin ad where  ad.id = mb.edit_by )'),
                edit_date: 'mb.edit_date',
                gender_name: 'gd.name_eng',
                blood_type_name: 'bt.name_eng'
            })
            .innerJoin('gender as gd', 'mb.gender', 'gd.id')
            .innerJoin('blood_type as bt', 'mb.blood_type', 'bt.id')
            .where('mb.status', iStatus)
            .andWhere('mb.email', email)
            .first()
    },
    getMemberById: async (con, params) => {
        const { id, email, status } = params
        
        const iStatus = status || 1

        return await con({ mb: 'member' })
            .select({
                id: 'mb.id',
                code: 'mb.code',
                number: 'mb.number',
                first_name: 'mb.first_name',
                last_name: 'mb.last_name',
                gender: 'mb.gender',
                email: 'mb.email',
                blood_type: 'mb.blood_type',
                birth_date: 'mb.birth_date',
                relationship: 'mb.relationship',
                identification_no: 'mb.identification_no',
                contact_no1: 'mb.contact_no1',
                contact_no2: 'mb.contact_no2',
                address1: 'mb.address1',
                address2: 'mb.address2',
                address3: 'mb.address3',
                username: 'mb.username',
                place_latitude: 'mb.place_latitude',
                place_longitude: 'mb.place_longitude',
                status: 'mb.status',
                create_date: 'mb.create_date',
                create_by: con.raw('( select concat( ad.first_name, \' \', ad.last_name ) from admin ad where  ad.id = mb.create_by )'),
                create_date: 'mb.create_date',
                edit_by: con.raw('( select concat( ad.first_name, \' \', ad.last_name ) from admin ad where  ad.id = mb.edit_by )'),
                edit_date: 'mb.edit_date',
                gender_name: 'gd.name_eng',
                blood_type_name: 'bt.name_eng',
                profile_image_id: 'mp.id',
                profile_image_path: 'mp.path',
                profile_image_name: 'mp.name',
                location_image_id: 'ml.id',
                location_image_path: 'ml.path',
                location_image_name: 'ml.name',
                qrcode_usage: con.raw('( select count(*) from creature as ct where ct.member_id = mb.id )')
            })
            .leftJoin('file as mp', function() { 
                this.on('mb.id', '=', 'mp.ref_id')
                this.andOnVal('mp.tag', '=', 'member-profile-image')
                this.andOnVal('mp.status', '=', 1)
            })
            .leftJoin('file as ml', function() { 
                this.on('mb.id', '=', 'ml.ref_id')
                this.andOnVal('ml.tag', '=', 'member-location-image')
                this.andOnVal('ml.status', '=', 1)
            })
            .innerJoin('gender as gd', 'mb.gender', 'gd.id')
            .innerJoin('blood_type as bt', 'mb.blood_type', 'bt.id')
            .where('mb.status', iStatus)
            .andWhere('mb.id', id)
            .first()
    },
    delete: async (con, params, user) => {
        return await con('member')
            .update({
                status: 2,
                edit_by: user.id,
                edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
            })
            .where('id', params.id)
    }
}