const dayjs = require('dayjs')
const moment = require('moment')
const ObjectUtil = require('../utils/ObjectUtil')

module.exports = {
    add: async (con, params, user) => {
        let rowNo = await con('treatment_history').select('number').orderBy('number', 'desc').first()
        let number = 0
        if (rowNo) number = rowNo.number

        const treatmentHistory = ObjectUtil.generateTreatmentHistoryForInsertToDb(params, user, number)
   
        return await con.transaction((trx)  => {
            con('treatment_history')
            .transacting(trx)
            .returning('id')
            .insert(treatmentHistory)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    update: async (con, params, user) => {
        const { id } = params

        const treatmentHistory = ObjectUtil.generateTreatmentHistoryForUpdateToDb(params, user)

        return await con.transaction(trx => {
            con('treatment_history')
            .update(treatmentHistory)
            .returning('id')
            .where('id', id)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    getTreatmentHistoryById: async (con, params) => {
        const { id, status } = params 
        const iStatus = status || 1
        
        return await con({ th: 'treatment_history' })
            .select({
                id: 'th.id',
                code: 'th.code',
                number: 'th.number',
                description: 'th.description',
                department: 'th.department',
                file_category: 'th.file_category',
                file_category_other: 'th.file_category_other',
                creature_id: 'th.creature_id',
                date: 'th.date',
                time: 'th.time',
                status: 'th.status',
                create_date: 'th.create_date',
                create_by: 'th.create_by',
                edit_date: 'th.edit_date',
                edit_by: 'th.edit_by',
                department_name: 'dm.name_eng',
                file_category_name: 'fc.name_eng'
            })
            .innerJoin('department as dm', 'th.department', 'dm.id')
            .innerJoin('file_category as fc', 'th.file_category', 'fc.id')
            .where('th.status', iStatus)
            .andWhere('th.id', id)
            .orderBy('th.number', 'desc')
            .first()
    },
    getTreatmentHistoryList: async (con, params) => {
        const { 
            creatureId, 
            departmentCriteria, 
            fileCategoryCriteria, 
            createDateCriteria, 
            limit, 
            offset, 
            orderByKey, 
            orderByValue, 
            status } = params

        const iOrderByKey = orderByKey || 'number'
        const iOrderByValue = orderByValue || 'asc'
        const iDepartmentCriteria = departmentCriteria || ''
        const iFileCategoryCriteria = fileCategoryCriteria || ''
        const iCreateDateCriteria = createDateCriteria || ''
        const iOffset = offset || 0
        const iLimit = limit || 10
        const iStatus = status || 1

        let whereFirst = iStatus !== null ? 'th.status = :status ' : ''

        if (iDepartmentCriteria && iDepartmentCriteria.trim().length > 0) {
            whereFirst += ' and dm.name_eng like :department '
        }

        if (iFileCategoryCriteria && iFileCategoryCriteria.trim().length > 0) {
            whereFirst += ' and fc.name_eng like :file_category '
        }

        if (moment(iCreateDateCriteria, 'YYYY/MM/DD').format('YYYY/MM/DD') === iCreateDateCriteria) {
            whereFirst += ' and cast (th.create_date as date) = :create_date '
        }

        return await con({ th: 'treatment_history'})
            .select(
                {
                    id: 'th.id',
                    code: 'th.code',
                    number: 'th.number',
                    description: 'th.description',
                    department: 'th.department',
                    department_name: 'dm.name_eng',
                    file_category: 'th.file_category',
                    file_category_other: 'th.file_category_other',
                    file_category_name: 'fc.name_eng',
                    date: 'th.date',
                    time: 'th.time',
                    creature_id: 'th.creature_id',
                    member_id: 'th.member_id',
                    status: 'th.status',
                    create_date: 'th.create_date',
                    create_by: 'th.create_by',
                    edit_date: 'th.edit_date',
                    edit_by: 'th.edit_by',
                }
            )
            .innerJoin('department as dm', 'th.department', 'dm.id')
            .innerJoin('file_category as fc', 'th.file_category', 'fc.id')
            .whereRaw(
                whereFirst, 
                {
                    status: iStatus,
                    department: iDepartmentCriteria + '%',
                    file_category: iFileCategoryCriteria + '%',
                    create_date: iCreateDateCriteria
                }
            )
            .andWhere('creature_id', creatureId)
            .orderBy('th.' + iOrderByKey, iOrderByValue)
            .limit(iLimit)
            .offset(iOffset * iLimit)
    },
    getTreatmentHistoryCount: async (con, params) => {
        const { 
            creatureId, 
            departmentCriteria, 
            fileCategoryCriteria, 
            createDateCriteria, 
            status } = params

        const iDepartmentCriteria = departmentCriteria || ''
        const iFileCategoryCriteria = fileCategoryCriteria || ''
        const iCreateDateCriteria = createDateCriteria || ''
        const iStatus = status || 1

        let whereFirst = iStatus !== null ? 'th.status = :status ' : ''

        if (iDepartmentCriteria && iDepartmentCriteria.trim().length > 0) {
            whereFirst += ' and dm.name_eng like :department '
        }

        if (iFileCategoryCriteria && iFileCategoryCriteria.trim().length > 0) {
            whereFirst += ' and fc.name_eng like :file_category '
        }

        if (moment(iCreateDateCriteria, 'YYYY/MM/DD').format('YYYY/MM/DD') === iCreateDateCriteria) {
            whereFirst += ' and cast (th.create_date as date) = :create_date '
        }

        return await con({ th: 'treatment_history'})
            .count('*')
            .innerJoin('department as dm', 'th.department', 'dm.id')
            .innerJoin('file_category as fc', 'th.file_category', 'fc.id')
            .whereRaw(
                whereFirst, 
                {
                    status: iStatus,
                    department: iDepartmentCriteria + '%',
                    file_category: iFileCategoryCriteria + '%',
                    create_date: iCreateDateCriteria
                }
            )
            .andWhere('creature_id', creatureId)
    },
    getTreatmentHistoryListByCreatureId: async (con, params) => {
        const { creature_id, status } = params 
        const iStatus = status || 1

        return await con({ th: 'treatment_history' })
            .select({
                id: 'th.id',
                code: 'th.code',
                number: 'th.number',
                description: 'th.description',
                department: 'th.department',
                department_name: 'dm.name_eng',
                file_category: 'th.file_category',
                file_category_other: 'th.file_category_other',
                file_category_name: 'fc.name_eng',
                date: 'th.date',
                time: 'th.time',
                creature_id: 'th.creature_id',
                member_id: 'th.member_id',
                status: 'th.status',
                create_date: 'th.create_date',
                create_by: 'th.create_by',
                edit_date: 'th.edit_date',
                edit_by: 'th.edit_by',
            })
            .innerJoin('department as dm', 'th.department', 'dm.id')
            .innerJoin('file_category as fc', 'th.file_category', 'fc.id')
            .where('th.status', iStatus)
            .andWhere('th.creature_id', creature_id)
            .orderBy('th.number', 'asc')
    },
    delete: async (con, params, user) => {
        return await con('treatment_history')
            .update({
                status: 2,
                edit_by: user.id,
                edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
            })
            .where('id', params.id)
    },
    deleteItems: async (con, params, user) => {
        return await con('treatment_history')
            .update({
                status: 2,
                edit_by: user.id,
                edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
            })
            .whereIn('id', params.id)
    },
    getTreatmentHistoryListWhereIn: async (con, params) => {
        const { items, status } = params 
        const iStatus = status || 1

        return await con({ th: 'treatment_history' })
            .select({
                id: 'th.id',
                code: 'th.code',
                number: 'th.number',
                description: 'th.description',
                department: 'th.department',
                department_name: 'dm.name_eng',
                file_category: 'th.file_category',
                file_category_name: 'fc.name_eng',
                date: 'th.date',
                time: 'th.time',
                creature_id: 'th.creature_id',
                member_id: 'th.member_id',
                status: 'th.status',
                create_date: 'th.create_date',
                create_by: 'th.create_by',
                edit_date: 'th.edit_date',
                edit_by: 'th.edit_by',
                image_path: 'mi.path',
                image_name: 'mi.name',
                image_path_si: 'si.path',
                image_name_si: 'si.name'
            })
            .leftJoin('file as mi', function() { 
                this.on('th.id', '=', 'mi.ref_id')
                this.andOnVal('mi.tag', '=', 'treatment-history-image')
                this.andOnVal('mi.status', '=', 1)
            })
            .leftJoin('file as si', function() { 
                this.on('th.id', '=', 'si.ref_id')
                this.andOnVal('si.tag', '=', 'treatment-history-image-list')
            })
            .innerJoin('department as dm', 'th.department', 'dm.id')
            .innerJoin('file_category as fc', 'th.file_category', 'fc.id')
            .whereIn('th.id', items)
            .andWhere('th.status', iStatus)
            .orderBy('th.number', 'asc')
    }
}