
const ObjectUtil = require('../utils/ObjectUtil')

module.exports = {
    authenticate: async (con, params) => {
        return await con.select('*').from('admin').where('username', params.username).andWhere('status', 1).first()
    },
    getAdminById: async (con, params) => {
        return await con.select('*').from('admin').where('id', params.id).andWhere('status', 1).first()
    },
    update: async (con, params, user) => {
        const { id } = params
        const admin = ObjectUtil.generateAdminForUpdateToDb(params)

        return await con.transaction(trx => {
            con('admin')
            .update(admin)
            .returning('*')
            .where('id', user.id)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    changePassword: async (con, params, user) => {
        const admin = await ObjectUtil.generateAdminChangePasswordForUpdateToDb(params, user)
        console.log('admin', admin)
        return await con.transaction(trx => {
            con('admin')
            .update(admin)
            .where('id', user.id)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    }
}