const dayjs = require('dayjs')
const moment = require('moment')
const ObjectUtil = require('../utils/ObjectUtil')


module.exports = {
    generate: async (con, params, user) => {
        const { amount } = params
        
        let rowNo = await con('qrcode_set').select('number').orderBy('number', 'desc').first()
        let number = 0
        if (rowNo) number = rowNo.number

        let lotNumber = await con('qrcode_set')
                    .count('*')
                    .where(con.raw(`cast (create_date as date) = ?`, dayjs().format('YYYY-MM-D')))
        
        const qrcodeSet = ObjectUtil.generateQRCodeSetForInsertToDb(params, user, number, parseInt(lotNumber[0].count))

        return await con.transaction((trx) => {
            con('qrcode_set').transacting(trx).returning('id').insert(qrcodeSet, 'id')
            .then( async (setId) => {
                let qrcodeItems = []
                
                let rowNo = await con('qrcode_item').select('number').orderBy('number', 'desc').first()
                let number = 0
                if (rowNo) number = rowNo.number
                
                for (var i = 0; i < amount; i++ ) {
                    const qrcodeItem = ObjectUtil.generateQRCodeItemForInsertToDb(setId, user, number + i, i + 1, lotNumber[0].count)
                    qrcodeItems.push(qrcodeItem)
                }

                return con('qrcode_item').insert(qrcodeItems).transacting(trx).returning('set_id')

            })
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    getQRCodeSetList: async (con, params) => {
        const { 
            limit, 
            offset, 
            orderByKey, 
            orderByValue, 
            status, 
            serialNoCriteria, 
            nameCriteria, 
            createDateCriteria } = params

        const iOrderByKey = orderByKey || 'number'
        const iOrderByValue = orderByValue || 'asc'
        const iSerialNoCriteria = serialNoCriteria || ''
        const iNameCriteria = nameCriteria || ''
        const iCreateDateCriteria = createDateCriteria || ''
        const iOffset = offset || 0
        const iLimit = limit || 10
        const iStatus = status || 1

        let whereFirst = iStatus !== null ? 'qs.status = :status ' : ''
        
        if (iSerialNoCriteria && iSerialNoCriteria.trim().length > 0) {
            whereFirst += ' and qs.code like :code '
        }

        if (iNameCriteria && iNameCriteria.trim().length > 0) {
            whereFirst += ' and qs.name like :name '
        }

        if (moment(iCreateDateCriteria, 'YYYY/MM/DD').format('YYYY/MM/DD') === iCreateDateCriteria) {

            whereFirst += ' and cast (qs.create_date as date) = :create_date '
        }

        return await con({ qs: 'qrcode_set'})
            .select(
                {
                    id: 'qs.id',
                    code: 'qs.code',
                    number: 'qs.number',
                    name: 'qs.name',
                    amount: 'qs.amount',
                    status: 'qs.status',
                    code: 'qs.code',
                    create_date: 'qs.create_date',
                    create_by: con.raw('concat( am.first_name, \' \', am.last_name )'),
                    code: 'qs.code'
                }
            )
            .whereRaw(
                whereFirst, 
                {
                    status: iStatus,
                    code: iSerialNoCriteria + '%', 
                    name: iNameCriteria + '%', 
                    create_date: iCreateDateCriteria
                }
            )
            .innerJoin('admin as am', 'am.id', 'qs.create_by')
            .orderBy('qs.' + iOrderByKey, iOrderByValue)
            .limit(iLimit)
            .offset(iOffset * iLimit)
    },
    getQRCodeSetCount: async (con, params) => {
        const { 
            status, 
            serialNoCriteria, 
            nameCriteria, 
            createDateCriteria } = params

        const iSerialNoCriteria = serialNoCriteria || ''
        const iNameCriteria = nameCriteria || ''
        const iCreateDateCriteria = createDateCriteria || ''
        const iStatus = status || 1

        let whereFirst = iStatus !== null ? 'qs.status = :status ' : ''
        
        if (iSerialNoCriteria && iSerialNoCriteria.trim().length > 0) {
            whereFirst += ' and qs.code like :code '
        }

        if (iNameCriteria && iNameCriteria.trim().length > 0) {
            whereFirst += ' and qs.name like :name '
        }

        if (moment(iCreateDateCriteria, 'YYYY/MM/DD').format('YYYY/MM/DD') === iCreateDateCriteria) {

            whereFirst += ' and cast (qs.create_date as date) = :create_date '
        }

        return await con({ qs: 'qrcode_set' })
            .count('*')
            .whereRaw(
                whereFirst, 
                {
                    status: iStatus,
                    code: iSerialNoCriteria + '%', 
                    name: iNameCriteria + '%', 
                    create_date: iCreateDateCriteria
                }
            ).innerJoin('admin as am', 'am.id', 'qs.create_by')
    },
    getQRCodeSetById: async (con, params) => {
        const { id, status } = params
        
        const iStatus = status || 1

        return await con({ qs: 'qrcode_set' })
            .select({
                id: 'qs.id',
                code: 'qs.code',
                number: 'qs.number',
                name: 'qs.name',
                description: 'qs.description', 
                amount: 'qs.amount',
                status: 'qs.status',
                create_date: 'qs.create_date',
                create_by: con.raw('( select concat( ad.first_name, \' \', ad.last_name ) from admin ad where  ad.id = qs.create_by )'),
                create_date: 'qs.create_date',
                edit_by: con.raw('( select concat( ad.first_name, \' \', ad.last_name ) from admin ad where  ad.id = qs.edit_by )'),
                edit_date: 'qs.edit_date',
                background_image_id: 'ql.id',
                background_image_path: 'ql.path',
                background_image_name: 'ql.name',
                register_used: con.raw('( select count(*) from member as mb where mb.qrcode_set_id = qs.id )'),
                creature_used: con.raw('( select count(*) from creature as ct where ct.qrcode_set_id = qs.id )'),
                used: con.raw('( select count(*) from qrcode_item as qi where not qi.usage_status = 1 and qi.set_id = qs.id )'),
                unused: con.raw('( select count(*) from qrcode_item as qi where qi.usage_status = 1 and qi.set_id = qs.id )')
            })
            .leftJoin('file as ql', function() { 
                this.on('qs.id', '=', 'ql.ref_id')
                this.andOnVal('ql.tag', '=', 'qrcode-set-background-image')
                this.andOnVal('ql.status', '=', 1)
            })
            .where('qs.status', iStatus)
            .andWhere('qs.id', id)
            .first()
    },
    getQRCodeItemsByParentId: async (con, params) => {
        const { parentId } = params

        return await con({ qi: 'qrcode_item' })
            .select({
                id: 'qi.id',
                code: 'qi.code',
                number: 'qi.number',
                status: 'qi.status',
                usage_status: 'qi.usage_status',
                code: 'qi.code',
                create_by: con.raw('( select concat( ad.first_name, \' \', ad.last_name ) from admin ad where  ad.id = qi.create_by )'),
                create_date: 'qi.create_date',
                edit_by: con.raw('( select concat( ad.first_name, \' \', ad.last_name ) from admin ad where  ad.id = qi.edit_by )'),
                edit_date: 'qi.create_date',
                code: 'qi.code'
            })
            .andWhere('set_id', parentId)
            .orderBy('number', 'asc')
    },
    getQRCodeItemsByIds: async (con, params) => {
        const { ids } = params
        return await con({ qi: 'qrcode_item' })
            .select({
                id: 'qi.id',
                code: 'qi.code',
                number: 'qi.number',
                status: 'qi.status',
                usage_status: 'qi.usage_status',
                code: 'qi.code',
                create_by: con.raw('( select concat( ad.first_name, \' \', ad.last_name ) from admin ad where  ad.id = qi.create_by )'),
                create_date: 'qi.create_date',
                edit_by: con.raw('( select concat( ad.first_name, \' \', ad.last_name ) from admin ad where  ad.id = qi.edit_by )'),
                edit_date: 'qi.create_date',
                code: 'qi.code'
            })
            .whereIn('qi.id', ids)
            .orderBy('number', 'asc')
    },
    updateQRCodeSet: async (con, params, user) => {
        const { id, name, description } = params 

        return await con.transaction((trx)  => {
            con('qrcode_set')
            .where('status', 1)
            .andWhere('id', id)
            .update({
                name: name,
                description: description,
                edit_by: user.id,
                edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
            })
            .returning('id')
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    updateQRCodeItem: async (con, params, user) => {
        const { id, usageStatus, status } = params 

        return await con.transaction((trx)  => {
            con('qrcode_item')
            .where('status', 1)
            .andWhere('id', id)
            .update({
                usage_status: usageStatus,
                status: status,
                edit_by: user.id,
                edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
            })
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    getQRCodeItemById: async (con, params) => {
        const { id, status } = params 

        const iStatus = status || 1

        return await con('qrcode_item')
            .select('*')
            .where('status', iStatus)
            .andWhere('id', id)
            .first()
    },
    countQRCodeAll: async (con) => {
        return await con({ qi: 'qrcode_item' })
            .count('*')
            .where('qi.status', 1)
            .andWhere('qs.status', 1)
            .innerJoin('qrcode_set as qs', 'qi.set_id', 'qs.id')
    },
    countQRCodeUsed: async (con) => {
        return await con('qrcode_item')
            .count('*')
            .where('status', 1)
            .whereNot('usage_status', 1)
    },
    getQRCodeItemList: async (con, params) => {
        const { 
            limit, 
            offset, 
            orderByKey, 
            orderByValue, 
            status, 
            serialNoCriteria, 
            setNoCriteria, 
            createDateCriteria, 
            setId } = params

        const iOrderByKey = orderByKey || 'number'
        const iOrderByValue = orderByValue || 'asc'
        const iSerialNoCriteria = serialNoCriteria || ''
        const iSetNoCriteria = setNoCriteria || ''
        const iCreateDateCriteria = createDateCriteria || ''
        const iOffset = offset || 0
        const iLimit = limit || 10
        const iStatus = status || 1
            
        let whereFirst = iStatus !== null ? 'qi.status = :status ' : ''
        
        if (iSerialNoCriteria && iSerialNoCriteria.trim().length > 0) {
            whereFirst += ' and qi.code like :code '
        }

        if (setId) {
            whereFirst += ' and qi.set_id = :set_id '
        }

        if (iSetNoCriteria && iSetNoCriteria.trim().length > 0) {
            whereFirst += ' and qs.code like :qs_code '
        }

        if (moment(iCreateDateCriteria, 'YYYY/MM/DD').format('YYYY/MM/DD') === iCreateDateCriteria) {
            whereFirst += ' and cast (qi.create_date as date) = :create_date '
        }

        return await con({ qi: 'qrcode_item'})
            .select(
                {
                    id: 'qi.id',
                    code: 'qi.code',
                    number: 'qi.number',
                    set_code: 'qs.code',
                    status: 'qi.status',
                    usage_status: 'qi.usage_status',
                    code: 'qi.code',
                    create_by: con.raw('( select concat( am.first_name, \' \', am.last_name ) from admin am where  am.id = qi.create_by )'),
                    create_date: 'qi.create_date',
                    edit_by: con.raw('( select concat( am.first_name, \' \', am.last_name ) from admin am where  am.id = qi.edit_by )'),
                    edit_date: 'qi.create_date',
                    code: 'qi.code'
                }
            )
            .whereRaw(
                whereFirst, 
                {
                    status: iStatus,
                    code: iSerialNoCriteria + '%',
                    set_id: setId,
                    qs_code: iSetNoCriteria,
                    create_date: iCreateDateCriteria
                }
            )
            .innerJoin('qrcode_set as qs', 'qi.set_id', 'qs.id')
            .orderBy('qi.' + iOrderByKey, iOrderByValue)
            .limit(iLimit)
            .offset(iOffset * iLimit)
    },
    getQRCodeItemCount: async (con, params) => {
        const { 
            status, 
            serialNoCriteria, 
            setId,
            setNoCriteria,
            createDateCriteria } = params

        const iSerialNoCriteria = serialNoCriteria || ''
        const iSetNoCriteria = setNoCriteria || ''
        const iCreateDateCriteria = createDateCriteria || ''
        const iStatus = status || 1

        let whereFirst = iStatus !== null ? 'qi.status = :status ' : ''
        
        if (iSerialNoCriteria && iSerialNoCriteria.trim().length > 0) {
            whereFirst += ' and qi.code like :code '
        }

        if (setId) {
            whereFirst += ' and qi.set_id = :set_id '
        }

        if (iSetNoCriteria && iSetNoCriteria.trim().length > 0) {
            whereFirst += ' and qs.code like :qs_code '
        }
        
        if (moment(iCreateDateCriteria, 'YYYY/MM/DD').format('YYYY/MM/DD') === iCreateDateCriteria) {
            whereFirst += ' and cast (qi.create_date as date) = :create_date '
        }

        return await con({ qi: 'qrcode_item' })
            .count('*')
            .whereRaw(
                whereFirst, 
                {
                    status: iStatus,
                    code: iSerialNoCriteria + '%',
                    set_id: setId,
                    qs_code: iSetNoCriteria,
                    create_date: iCreateDateCriteria
                }
            )
            .innerJoin('qrcode_set as qs', 'qi.set_id', 'qs.id')
    }
}