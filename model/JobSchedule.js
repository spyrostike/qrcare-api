module.exports = {
    getJobScheduleById: async (con, params) => {
        return await con('job_schedule')
            .select('*')
            .where('id', params.id)
            .andWhere('status_working', 1)
            .andWhere('status', 1)
            .first()
    }
}