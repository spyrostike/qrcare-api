const dayjs = require('dayjs')
const ObjectUtil = require('../utils/ObjectUtil')

module.exports = {
    add: async (con, params) => {
        const file = ObjectUtil.generateFileForInsertToDb(params)
        
        return await con.transaction(trx => {
            con('file')
            .returning('*')
            .insert(file)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    setFilesRefId: async (con, params) => {
        const { ids, refId } = params

        return await con.transaction(trx => {
            con('file')
            .update({ 
                ref_id: refId, 
                edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
            })
            .whereIn('id', ids)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    getFileList: async (con, params) => {
        const { refId, limit, offset, orderBy, tag, status } = params 
        const iStatus = status || 1

        const iOffset = offset || 0
        const iLimit = limit || 10
        const iOrderBy = orderBy || 'acs'

        return await con('file')
            .select('*')
            .where('status', iStatus)
            .andWhere('ref_id', refId)
            .andWhere('tag', tag)
            .orderBy('create_date', iOrderBy)
            .limit(iLimit)
            .offset(iOffset * iLimit)
    },
    getFileCountByRefIdAndTag: async (con, params) => {
        const { refId, tag, status } = params
        const iStatus = status || 1

        return await con('file')
            .count('*')
            .where('status', iStatus)
            .andWhere('ref_id', refId)
            .andWhere('tag', tag)
    },
    getFileById: async (con, params) => {
        const { id, status } = params
        const iStatus = status || 1

        return await con('file')
            .select('*')
            .where('status', iStatus)
            .andWhere('id', id)
            .orderBy('create_date', 'desc')
            .first()
    },
    getFileByRefIdAndTag: async (con, params) => {
        const { refId, tag, status } = params
        const iStatus = status || 1

        return await con('file')
            .select('*')
            .where('status', iStatus)
            .andWhere('ref_id', refId)
            .andWhere('tag', tag)
            .orderBy('create_date', 'desc')
            .first()
    },
    setRefId: async (con, params) => {
        const { id, refId } = params

        return await con.transaction(trx => {
            con('file')
            .update({ 
                ref_id: refId, 
                edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
            })
            .where('id', id)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    delete: async (con, params, user) => {
        return await con('file')
            .update({
                status: 2,
                edit_by: user.id,
                edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
            })
            .where('id', params.id)
    },
    deleteItems: async (con, params, user) => {
        return await con('file')
            .update({
                status: 2,
                edit_by: user.id,
                edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
            })
            .whereIn('id', params.id)
    }
}