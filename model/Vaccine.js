const dayjs = require('dayjs')
const moment = require('moment')
const ObjectUtil = require('../utils/ObjectUtil')

module.exports = {
    ///////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////// vaccine book ////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////

    addBook: async (con, params, user) => {
        let rowNo = await con('vaccine_book').select('number').orderBy('number', 'desc').first()
        let number = 0
        if (rowNo) number = rowNo.number

        const vaccineBook = ObjectUtil.generateVaccineBookForInsertToDb(params, user, number)

        return await con.transaction((trx)  => {
            con('vaccine_book')
            .transacting(trx)
            .returning('id')
            .insert(vaccineBook)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    updateBook: async  (con, params, user) => {
        const { id } = params

        const vaccineBook = ObjectUtil.generateVaccineBookForUpdateToDb(params, user)

        return await con.transaction(trx => {
            con('vaccine_book')
            .update(vaccineBook)
            .where('id', id)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    deleteBook: async (con, params, user) => {
        return await con('vaccine_book')
            .update({
                status: 2,
                edit_by: user.id,
                edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
            })
            .where('id', params.id)
    },
    getVaccineBookList: async (con, params) => {
        const { 
            creatureId, 
            limit, 
            offset, 
            orderByKey, 
            orderByValue, 
            status, 
            nameCriteria, 
            createDateCriteria } = params

        const iOrderByKey = orderByKey || 'number'
        const iOrderByValue = orderByValue || 'asc'
        const iNameCriteria = nameCriteria || ''
        const iCreateDateCriteria = createDateCriteria || ''
        const iOffset = offset || 0
        const iLimit = limit || 10
        const iStatus = status || 1
        
        let whereFirst = iStatus !== null ? 'vb.status = :status ' : ''

        if (iNameCriteria && iNameCriteria.trim().length > 0) {
            whereFirst += ' and vt.name_eng like :name '
        }

        if (moment(iCreateDateCriteria, 'YYYY/MM/DD').format('YYYY/MM/DD') === iCreateDateCriteria) {
            whereFirst += ' and cast (vb.create_date as date) = :create_date '
        }
        
        return await con({ vb: 'vaccine_book' })
            .select({
                id: 'vb.id',
                code: 'vb.code',
                vaccine_type: 'vb.vaccine_type',
                other: 'vb.other',
                creature_id: 'vb.creature_id',
                member_id: 'vb.member_id',
                status: 'vb.status',
                create_date: 'vb.create_date',
                create_by: 'vb.create_by',
                edit_date: 'vb.edit_date',
                edit_by: 'vb.edit_by',
                vaccine_type_name: 'vt.name_eng'
            })
            .whereRaw(
                whereFirst, 
                {
                    status: iStatus,
                    name: iNameCriteria + '%',
                    create_date: iCreateDateCriteria
                }
            )
            .andWhere('vb.creature_id', creatureId)
            .innerJoin('vaccine_type as vt', 'vb.vaccine_type', 'vt.id')
            .orderBy('vb.' + iOrderByKey, iOrderByValue)
            .limit(iLimit)
            .offset(iOffset * iLimit)
    },
    getVaccineBookCount: async (con, params) => {
        const { creatureId, nameCriteria, createDateCriteria, status } = params

        const iNameCriteria = nameCriteria || ''
        const iCreateDateCriteria = createDateCriteria || ''
        const iStatus = status || 1

        let whereFirst = iStatus !== null ? 'vb.status = :status ' : ''

        if (iNameCriteria && iNameCriteria.trim().length > 0) {
            whereFirst += ' and vt.name_eng like :name '
        }

        if (moment(iCreateDateCriteria, 'YYYY/MM/DD').format('YYYY/MM/DD') === iCreateDateCriteria) {
            whereFirst += ' and cast (vb.create_date as date) = :create_date '
        }

        return await con({ vb: 'vaccine_book' })
            .count('*')
            .whereRaw(
                whereFirst, 
                {
                    status: iStatus,
                    name: iNameCriteria + '%',
                    create_date: iCreateDateCriteria
                }
            )
            .innerJoin('vaccine_type as vt', 'vb.vaccine_type', 'vt.id')
            .andWhere('vb.creature_id', creatureId)
    },
    getVaccineBookListByCreatureId: async (con, params) => {
        const { creature_id, status } = params 
        const iStatus = status || 1
        return await con({ vb: 'vaccine_book' })
            .select({
                id: 'vb.id',
                code: 'vb.code',
                vaccine_type: 'vb.vaccine_type',
                other: 'vb.other',
                creature_id: 'vb.creature_id',
                member_id: 'vb.member_id',
                status: 'vb.status',
                create_date: 'vb.create_date',
                create_by: 'vb.create_by',
                edit_date: 'vb.edit_date',
                edit_by: 'vb.edit_by',
                vaccine_type_name: 'vt.name_eng'
            })
            .innerJoin('vaccine_type as vt', 'vb.vaccine_type', 'vt.id')
            .where('vb.status', iStatus)
            .andWhere('vb.creature_id', creature_id)
            .orderBy('vb.number', 'asc')
    },
    getVaccineBookById: async (con, params) => {
        const { id, status } = params 
        const iStatus = status || 1
        
        return await con({ vb: 'vaccine_book' })
            .select({
                id: 'vb.id',
                code: 'vb.code',
                number: 'vb.number',
                vaccine_type: 'vb.vaccine_type',
                other: 'vb.other',
                creature_id: 'vb.creature_id',
                member_id: 'vb.member_id',
                status: 'vb.status',
                create_date: 'vb.create_date',
                create_by: 'vb.create_by',
                edit_date: 'vb.edit_date',
                edit_by: 'vb.edit_by',
                vaccine_type_name: 'vt.name_eng'
            })
            .innerJoin('vaccine_type as vt', 'vb.vaccine_type', 'vt.id')
            .where('vb.status', iStatus)
            .andWhere('vb.id', id)
            .orderBy('vb.number', 'desc')
            .first()
    },
    deleteBookItems: async (con, params, user) => {
        return await con('vaccine_book')
            .update({
                status: 2,
                edit_by: user.id,
                edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
            })
            .whereIn('id', params.id)
    },

    ///////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////// vaccine ///////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////

    add: async (con, params, user) => {
        let rowNo = await con('vaccine').select('number').orderBy('number', 'desc').first()
        let number = 0
        if (rowNo) number = rowNo.number

        const vaccine = ObjectUtil.generateVaccineForInsertToDb(params, user, number)
       
        return await con.transaction((trx)  => {
            con('vaccine')
            .transacting(trx)
            .returning('id')
            .insert(vaccine)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    update: async (con, params, user) => {
        const { id } = params
        const vaccine = ObjectUtil.generateVaccineForUpdateToDb(params, user)
        
        return await con.transaction((trx)  => {
            con('vaccine')
            .transacting(trx)
            .update(vaccine)
            .returning('id')
            .where('id', id)
            .then(trx.commit)
            .catch(trx.rollback)
        })
    },
    getVaccineListByBookId: async (con, params) => {
        const { vaccine_book_id, status } = params 
        const iStatus = status || 1
        
        return await con('vaccine')
            .select('*')
            .where('status', iStatus)
            .andWhere('vaccine_book_id', vaccine_book_id)
            .orderBy('number', 'asc')
    },
    getVaccineList: async (con, params) => {
        const { 
            bookId, 
            hospitalCriteria, 
            createDateCriteria, 
            orderByKey, 
            orderByValue, 
            offset, 
            limit, 
            status } = params 
        
        const iOrderByKey = orderByKey || 'number'
        const iOrderByValue = orderByValue || 'asc'
        const iHospitalCriteria = hospitalCriteria || ''
        const iCreateDateCriteria = createDateCriteria || ''
        const iOffset = offset || 0
        const iLimit = limit || 10
        const iStatus = status || 1

        let whereFirst = iStatus !== null ? 'vc.status = :status ' : ''
        
        if (iHospitalCriteria && iHospitalCriteria.trim().length > 0) {
            whereFirst += ' and vc.hospital_name like :hospital_name '
        }   
        
        if (moment(iCreateDateCriteria, 'YYYY/MM/DD').format('YYYY/MM/DD') === iCreateDateCriteria) {
            whereFirst += ' and cast (vc.create_date as date) = :create_date '
        }

        return await con({ vc: 'vaccine' })
            .select('*')
            .whereRaw(
                whereFirst, 
                {
                    status: iStatus,
                    hospital_name: iHospitalCriteria + '%',
                    create_date: iCreateDateCriteria
                }
            )
            .andWhere('vc.vaccine_book_id', bookId)
            .orderBy('vc.' + iOrderByKey, iOrderByValue)
            .limit(iLimit)
            .offset(iOffset * iLimit)
    },
    getVaccineCount: async (con, params) => {
        const { 
            bookId, 
            hospitalCriteria, 
            createDateCriteria, 
            status } = params 

        const iHospitalCriteria = hospitalCriteria || ''
        const iCreateDateCriteria = createDateCriteria || ''
        const iStatus = status || 1

        let whereFirst = iStatus !== null ? 'vc.status = :status ' : ''
        
        if (iHospitalCriteria && iHospitalCriteria.trim().length > 0) {
            whereFirst += ' and vc.hospital_name like :hospital_name '
        }   
        
        if (moment(iCreateDateCriteria, 'YYYY/MM/DD').format('YYYY/MM/DD') === iCreateDateCriteria) {
            whereFirst += ' and cast (vc.create_date as date) = :create_date '
        }

        return await con({ vc: 'vaccine' })
            .count('*')
            .whereRaw(
                whereFirst, 
                {
                    status: iStatus,
                    hospital_name: iHospitalCriteria + '%',
                    create_date: iCreateDateCriteria
                }
            )
            .andWhere('vc.vaccine_book_id', bookId)
    },
    getVaccineById: async (con, params) => {
        const { id, status } = params 
        const iStatus = status || 1

        return await con('vaccine')
            .select('*')
            .where('status', iStatus)
            .andWhere('id', id)
            .first()
    },
    delete: async (con, params, user) => {
        return await con('vaccine')
            .update({
                status: 2,
                edit_by: user.id,
                edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
            })
            .where('id', params.id)
    }
}