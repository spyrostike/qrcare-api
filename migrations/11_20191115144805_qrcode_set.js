const uuid = require('uuidv4').default

exports.up = function(knex) {
    return knex.schema.createTable('qrcode_set', function(table) {
        // table.increments('id')
        table.uuid('id').primary()
        table.string('code', 15).unique()
        table.integer('number').unsigned()
        table.string('name', 150).notNullable()
        table.integer('amount').unsigned()
        table.text('description')
        table.integer('status').notNullable()
        table.datetime('create_date', { precision: 6 }).defaultTo(knex.fn.now(6))
        table.uuid('create_by')
        table.datetime('edit_date', { precision: 6 }).defaultTo(knex.fn.now(6))
        table.uuid('edit_by')
            
        table.foreign('status').references('id').inTable('status')
    })
}

exports.down = function(knex) {}
