exports.up = function(knex) {
    return knex.schema.createTable('vaccine_book', function(table) {
        table.uuid('id').primary()
        table.string('code', 15).unique()
        table.integer('number').unsigned()
        table.integer('vaccine_type').unsigned().notNullable()
        table.string('other', 150)
        table.uuid('creature_id').notNullable()
        table.uuid('member_id').notNullable()
        table.integer('status', 1).notNullable()
        table.datetime('create_date', { precision: 6, useTz: true }).defaultTo(knex.fn.now(6))
        table.uuid('create_by')
        table.datetime('edit_date', { precision: 6, useTz: true }).defaultTo(knex.fn.now(6))
        table.uuid('edit_by')
        
        table.foreign('vaccine_type').references('id').inTable('vaccine_type')
        table.foreign('creature_id').references('id').inTable('creature')
        table.foreign('member_id').references('id').inTable('member')
        table.foreign('status').references('id').inTable('status')
    })
}

exports.down = function(knex) {}
