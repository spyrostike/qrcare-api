const uuid = require('uuidv4').default

exports.up = function(knex) {
    return knex.schema.createTable('job_schedule', function(table) {
        // table.increments('id')
        table.increments('id')
        table.string('name_eng', 100).notNullable()
        table.string('name_th', 100).notNullable()
        table.string('run_schedule')
        table.text('description')
        table.integer('status_working').notNullable()
        table.integer('status').notNullable()
        table.datetime('create_date', { precision: 6 }).defaultTo(knex.fn.now(6))
        table.uuid('create_by')
        table.datetime('edit_date', { precision: 6 }).defaultTo(knex.fn.now(6))
        table.uuid('edit_by')
        
        table.foreign('status').references('id').inTable('status')
    }).then(function () {
        return knex('job_schedule').insert(
            [
                { 
                    id: 1, 
                    name_eng: 'send email job', 
                    name_th: 'ส่งอีเมล์', 
                    run_schedule: 'every 30 secconds', 
                    description: 'Send email every 30 seconds',
                    status_working: 1,
                    status: 1
                },
                { 
                    id: 2, 
                    name_eng: 'remove file unuse job', 
                    name_th: 'ลบไฟล์ที่ไม่มีการใช้งาน', 
                    run_schedule: 'every days at 00.15 AM', 
                    description: 'Remove unuse file every days at 00.15 AM ',
                    status_working: 1,
                    status: 1
                }
            ]
        )
        
    })
}

exports.down = function(knex) {}
