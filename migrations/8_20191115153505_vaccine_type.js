exports.up = function(knex) {
    return knex.schema.createTable('vaccine_type', function(table) {
        table.increments('id')
        table.string('name_eng', 100).notNullable()
        table.string('name_th', 100).notNullable()
        table.datetime('create_date', { precision: 6 }).defaultTo(knex.fn.now(6))
        table.uuid('create_by')
        table.datetime('edit_date', { precision: 6 }).defaultTo(knex.fn.now(6))
        table.uuid('edit_by')

    }).then(function () {
        return knex('vaccine_type').insert(
            [
                { id: 1, name_eng: 'BCG', name_th: 'BCG' },
                { id: 2, name_eng: 'HBV', name_th: 'HBV' },
                { id: 3, name_eng: 'Dtap, Tdap', name_th: 'Dtap, Tdap' },
                { id: 4, name_eng: 'IPV', name_th: 'IPV' },
                { id: 5, name_eng: 'Live JE', name_th: 'Live JE' },
                { id: 6, name_eng: 'Hib', name_th: 'Hib' },
                { id: 7, name_eng: 'HAV', name_th: 'HAV' },
                { id: 8, name_eng: 'VZV or MMRV', name_th: 'VZV or MMRV' },
                { id: 9, name_eng: 'Influenza', name_th: 'Influenza' },
                { id: 10, name_eng: 'PCV', name_th: 'PCV' },
                { id: 11, name_eng: 'Rota', name_th: 'Rota' },
                { id: 12, name_eng: 'HPV', name_th: 'HPV' },
                { id: 13, name_eng: 'DEN', name_th: 'DEN' },
                { id: 14, name_eng: 'Other', name_th: 'Other' }
            ]
        )
        
    })
}

exports.down = function(knex) {}
