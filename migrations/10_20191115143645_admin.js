const uuid = require('uuidv4').default

exports.up = function(knex) {
    return knex.schema.createTable('admin', function(table) {
        // table.increments('id')
        table.uuid('id').primary()
        table.string('code', 15).unique()
        table.string('username', 50).unique().notNullable()
        table.string('password', 150).notNullable()
        table.string('private_key', 100).notNullable()
        table.string('first_name', 50).notNullable()
        table.string('last_name', 50).notNullable()
        table.string('identity_no', 50).notNullable()
        table.integer('gender', 1).notNullable()
        table.string('email', 100).unique().notNullable()
        table.string('contact_no', 10).notNullable()
        table.string('address', 10).nullable()
        table.integer('role').notNullable()
        table.integer('status').notNullable()
        table.datetime('create_date', { precision: 6 }).defaultTo(knex.fn.now(6))
        table.uuid('create_by')
        table.datetime('edit_date', { precision: 6 }).defaultTo(knex.fn.now(6))
        table.uuid('edit_by')

        table.foreign('status').references('id').inTable('status')
        table.foreign('role').references('id').inTable('role')
        table.foreign('gender').references('id').inTable('gender')
            
    }).then(function () {
        return knex('admin').insert(
            { 
                id: uuid(),
                code: '000000000000001',
                first_name: 'ผู้ดูแลระบบ QRCare',
                last_name: 'นามสกุล',
                identity_no: '1111111111111',
                gender: 1,
                email: 'xxx@gmail.com',
                contact_no: '0812345678',
                address: 'n/a',
                username: 'administrator',
                password: '$2b$10$njciKPbFZBe63n1l/fIeluwejXVvUxUBE5KCFi7aIOULn.t9Ez2DO',
                private_key: 'YYLmfY6IehjZMQA',
                role: 1,
                status: 1
            }
        )
        
    })
}

exports.down = function(knex) {}
