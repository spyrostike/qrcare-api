const uuid = require('uuidv4').default

exports.up = function(knex) {
    return knex.schema.createTable('mail_status', function(table) {
        // table.increments('id')
        table.increments('id')
        table.string('name_eng', 100).notNullable()
        table.string('name_th', 100).notNullable()
        table.datetime('create_date', { precision: 6 }).defaultTo(knex.fn.now(6))
        table.uuid('create_by')
        table.datetime('edit_date', { precision: 6 }).defaultTo(knex.fn.now(6))
        table.uuid('edit_by')
            
    }).then(function () {
        return knex('mail_status').insert(
            [
                {
                    id: 1,
                    name_eng: 'waiting_for_send',
                    name_th: 'รอทำการส่ง'
                },
                { 
                    id: 2,
                    name_eng: 'sent_successfully',
                    name_th: 'ทำการส่งสำเร็จ'
                },
                { 
                    id: 3,
                    name_eng: 'sent_failed',
                    name_th: 'การส่งล้มเหลว'
                }
            ]
        )
        
    })
}

exports.down = function(knex) {}
