exports.up = function(knex) {
    return knex.schema.createTable('mail', function(table) {
        table.uuid('id').primary()
        table.string('from', 150).notNullable()
        table.string('to', 150)
        table.string('subject', 200)
        table.text('html_text')
        table.text('description')
        table.string('tag', 100)
        table.integer('mail_status', 1).notNullable()
        table.datetime('sent_time', { precision: 6, useTz: true })
        table.string('error_message', 500)
        table.integer('status', 1).notNullable()
        table.datetime('create_date', { precision: 6, useTz: true }).defaultTo(knex.fn.now(6))
        table.uuid('create_by')
        table.datetime('edit_date', { precision: 6, useTz: true }).defaultTo(knex.fn.now(6))
        table.uuid('edit_by')

        table.foreign('status').references('id').inTable('status')
        table.foreign('mail_status').references('id').inTable('mail_status')
    })
}

exports.down = function(knex) {}
