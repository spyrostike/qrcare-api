exports.up = function(knex) {
    return knex.schema.createTable('qrcode_item', function(table) {
        // table.increments('id')
        table.uuid('id').primary()
        table.string('code', 15).unique()
        table.integer('number').unsigned()
        table.uuid('set_id')
        table.integer('usage_status')
        table.integer('status').notNullable()
        table.datetime('create_date', { precision: 6 }).defaultTo(knex.fn.now(6))
        table.uuid('create_by')
        table.datetime('edit_date', { precision: 6 }).defaultTo(knex.fn.now(6))
        table.uuid('edit_by')

        table.foreign('set_id').references('id').inTable('qrcode_set')
        table.foreign('usage_status').references('id').inTable('usage_status')
        table.foreign('status').references('id').inTable('status')
    })
}

exports.down = function(knex) {}
