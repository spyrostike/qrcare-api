exports.up = function(knex) {
    return knex.schema.createTable('creature', function(table) {
        table.uuid('id').primary()
        table.string('code', 15).unique()
        table.integer('number').unsigned()
        table.string('first_name', 50).notNullable()
        table.string('last_name', 50).notNullable()
        table.integer('gender').notNullable()
        table.integer('blood_type').notNullable()
        table.date('birth_date').notNullable()
        table.decimal('weight', 6, 2)
        table.decimal('height', 6, 2)
        table.uuid('member_id')
        table.uuid('qrcode_set_id')
        table.uuid('qrcode_item_id')
        table.integer('status', 1).notNullable()
        table.datetime('create_date', { precision: 6 }).defaultTo(knex.fn.now(6))
        table.uuid('create_by')
        table.datetime('edit_date', { precision: 6 }).defaultTo(knex.fn.now(6))
        table.uuid('edit_by')

        table.foreign('gender').references('id').inTable('gender')
        table.foreign('blood_type').references('id').inTable('blood_type')
        table.foreign('qrcode_set_id').references('id').inTable('qrcode_set')
        table.foreign('qrcode_item_id').references('id').inTable('qrcode_item')
        table.foreign('member_id').references('id').inTable('member')
        table.foreign('status').references('id').inTable('status')
    })
}

exports.down = function(knex) {}
