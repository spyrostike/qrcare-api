// main.js
// const nodemailer = require('nodemailer')

// setup mail transporter service
// const transporter = nodemailer.createTransport({
//     service: 'gmail',
//     auth: {
//         user: 'doug.yancey.funnie32@gmail.com', // your email
//         pass: 'Tanat@175.2020'              // your password
//     }
// })

// setup email data with unicode symbols
// const mailOptions = {
//   from: 'sender@hotmail.com',              // sender
//   to: 'receiver@hotmail.com',              // list of receivers
//   subject: 'Hello from sender',            // Mail subject
//   html: '<b>Do you receive this mail?</b>' // HTML body
// }

const sgMail = require('@sendgrid/mail')

sgMail.setApiKey(process.env.SENDGRID_API_KEY)
// console.log('SENDGRID_API_KEY', process.env.SENDGRID_API_KEY)

// const msg = {
//     to: 'spyrostike@gmail.com',
//     from: 'qrcare.official2020@gmail.com',
//     subject: 'Sending with Twilio SendGrid is Fun',
//     text: 'and easy to do anywhere, even with Node.js',
//     html: '<strong>and easy to do anywhere, even with Node.js</strong>',
// };


// sgMail.send(msg).then(() => {
//     console.log('Message sent')
// }).catch((error) => {
//     console.log(error.response.body)
//     // console.log(error.response.body.errors[0].message)
// })

module.exports = {
    // sendMail: async (mailOptions) => {
    //     return await transporter.sendMail(mailOptions)
    // },
    sendMail: async (msg) => {
        return await sgMail.send(msg)
    }
}