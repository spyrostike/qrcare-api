const cron = require('node-cron')
const con = require('../config/db.js')
const mailSender = require('../services/mailSender')
const JobSchedule = require('../model/JobSchedule')
const Mail = require('../model/Mail')

var sendMailJob = cron.schedule('*/60 * * * * *', async () => {
    
    try {
        const jobSchedule = await JobSchedule.getJobScheduleById(con, { id: 1 })

        if (!jobSchedule) return
        if (jobSchedule.status_working !== 1) return

        const list = await Mail.getMailListUnsendSuccess(con)
        // sendMailJob.stop()

        for (var i = 0; i < list.length; i++) {

            let successItems = []

            try {
                const mailOptions = {
                    from: 'qrcare.official2020@gmail.com',              // sender
                    to: list[i].to,              // list of receivers
                    subject: list[i].subject,   
                    text: list[i].html_text,         // Mail subject
                    html:  list[i].html_text // HTML body
                }

                await mailSender.sendMail(mailOptions)
                successItems.push(list[i].id)

            } catch (error) {
                console.log('error', error.response)
                await Mail.updateMailFailed(con, { id: list[i].id, error_message: error.response.body.errors[0] })
            } 

            if (successItems.length > 0) await Mail.updateMailItemsSuccess(con, { items: successItems })
        }
    } catch (error) {
        console.log('error', error)
    }
})

// var fileUnUseJob = cron.schedule('*/5 * * * * *', async () => {
//     // console.log('remove file unuse')
// })

sendMailJob.start()
// fileUnUseJob.start()
