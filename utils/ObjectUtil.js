const uuid = require('uuidv4').default
const bcrypt = require('bcrypt')
const sqlHelper = require('../utils/sqlHelper')
const dayjs = require('dayjs')

module.exports = {
    generateAdminForUpdateToDb: (params) => {
        const { 
            firstName,
            lastName,
            gender } = params

        return {
            first_name: firstName,
            last_name: lastName,
            gender: gender,
            edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
        }
    },
    generateAdminChangePasswordForUpdateToDb: async(params) => {
        const { newPassword } = params

        const privateKey = await bcrypt.genSalt(15)
        const encodePassword = await bcrypt.hash(newPassword + privateKey, privateKey)

        return {
            password: encodePassword,
            private_key: privateKey,
            edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
        }
    },
    generateMemberForInsertToDb: async (params, number) => {
        const { 
            qrcodeSetId,
            qrcodeItemId,
            username,
            password,
            firstName,
            lastName,
            gender,
            email,
            bloodType,
            birthDate,
            relationship,
            identificationNo,
            contactNo1,
            contactNo2,
            address1,
            address2,
            address3,
            placeLatitude,
            placeLongitude } = params
        
        const privateKey = await bcrypt.genSalt(15)
        const encodePassword = await bcrypt.hash(password + privateKey, privateKey)

        return {
            id: uuid(),
            code: sqlHelper.autoFill((number + 1).toString(), '0', 15, 'MB'),
            number: number + 1,
            username: username,
            password: encodePassword,
            private_key: privateKey,
            first_name: firstName,
            last_name: lastName,
            gender: gender,
            email: email,
            blood_type: bloodType,
            birth_date: birthDate,
            relationship: relationship,
            identification_no: identificationNo,
            contact_no1: contactNo1,
            contact_no2: contactNo2,
            address1: address1,
            address2: address2,
            address3: address3,
            qrcode_set_id: qrcodeSetId,
            qrcode_item_id: qrcodeItemId,
            place_latitude: placeLatitude,
            place_longitude: placeLongitude,
            status: 1
        }
    },
    generateMemberForUpdateToDb: (params, user) => {
        const { 
            firstName,
            lastName,
            gender,
            email,
            bloodType,
            birthDate,
            relationship,
            identificationNo,
            contactNo1,
            contactNo2,
            address1,
            address2,
            address3,
            status } = params

        return {
            first_name: firstName,
            last_name: lastName,
            gender: gender,
            email: email,
            blood_type: bloodType,
            birth_date: birthDate,
            relationship: relationship,
            identification_no: identificationNo,
            contact_no1: contactNo1,
            contact_no2: contactNo2,
            address1: address1,
            address2: address2,
            address3: address3,
            status: status,
            edit_by: user.id,
            edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
        }
    },
    generateMemberProfileForUpdateToDb: (params, user) => {
        const { 
            firstName,
            lastName,
            email,
            gender,
            bloodType,
            birthDate,
            relationship,
            identificationNo,
            contactNo1,
            contactNo2,
            address1,
            address2,
            address3 } = params
            
        return {
            first_name: firstName,
            last_name: lastName,
            email: email,
            gender: gender,
            blood_type: bloodType,
            birth_date: birthDate,
            relationship: relationship,
            identification_no: identificationNo,
            contact_no1: contactNo1,
            contact_no2: contactNo2,
            address1: address1,
            address2: address2,
            address3: address3,
            edit_by: user.id,
            edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
        }
    },
    generateMemberLocationForUpdateToDb: (params, user) => {
        const { placeLatitude, placeLongitude} = params
            
        return {
            place_latitude: placeLatitude,
            place_longitude: placeLongitude,
            edit_by: user.id,
            edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
        }
    },
    generateMemberChangePasswordForUpdateToDb: async (params, user) => {
        const { newPassword } = params

        const privateKey = await bcrypt.genSalt(15)
        const encodePassword = await bcrypt.hash(newPassword + privateKey, privateKey)

        return {
            password: encodePassword,
            private_key: privateKey,
            edit_by: user.id,
            edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
        }
    },
    generateFileForInsertToDb: (params) => {
        const { 
            refId,
            tag,
            filename,
            destination,
            mimetype,
            size,
            type } = params

        let path = destination.split('/')
        path = path[path.length - 1]
        
        return {
            id: uuid(),
            ref_id: refId,
            tag: tag,
            path: path,
            name: filename,
            type: type,
            sub_type: mimetype,
            size: size,
            status: 1
        }
    },
    generateQRCodeSetForInsertToDb: (params, user, number, codeNumber) => {
        const { name, amount, description } = params

        return {
            id: uuid(),
            code: sqlHelper.autoFill(((codeNumber + 1) + dayjs().format('DDMMYYYY')).toString(), '0', 15, 'QS'),
            number: number + 1,
            name: name, 
            amount: amount,
            status: 1,
            description: description,
            create_by: user.id,
            edit_by: user.id
        }
    },

    generateQRCodeItemForInsertToDb: (setId, user, number, numberInLot, codeNumber) => {
        return {
            id: uuid(),
            code: sqlHelper.autoFill( numberInLot + sqlHelper.autoFill(codeNumber, '0', 2) + dayjs().format('DDMMYYYY').toString(), '0', 15),
            number: parseInt(number) + 1,
            usage_status: 1,
            set_id: setId[0],
            status: 1,
            create_by: user.id,
            edit_by: user.id
        }
    },
    generateCreatureForInsertToDb: (params, user, number) => {
        const { 
            qrcodeSetId,
            qrcodeItemId,
            firstName,
            lastName,
            gender,
            bloodType,
            birthDate,
            weight,
            height } = params

         return {
            id: uuid(),
            code: sqlHelper.autoFill((number + 1).toString(), '0', 15, 'CT'),
            number: number + 1,
            first_name: firstName,
            last_name: lastName,
            gender: gender,
            blood_type: bloodType,
            birth_date: birthDate,
            weight: weight,
            height: height,
            qrcode_set_id: qrcodeSetId,
            qrcode_item_id: qrcodeItemId,
            member_id: user.id,
            status: 1,
            create_by: user.id,
            edit_by: user.id
        }
    },
    generateCreatureForUpdateToDb: (params, user) => {
        const { 
            firstName,
            lastName,
            gender,
            bloodType,
            birthDate,
            weight,
            height } = params
            
         return {
            first_name: firstName,
            last_name: lastName,
            gender: gender,
            blood_type: bloodType,
            birth_date: birthDate,
            weight: weight,
            height: height,
            edit_by: user.id,
            edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
        }
    },
    generateTreatmentHistoryForInsertToDb: (params, user, number) => {
        const { 
            date,
            time,
            description,
            department,
            fileCategory,
            fileCategoryOther: fileCategoryOther,
            creatureId } = params

         return {
            id: uuid(),
            code: sqlHelper.autoFill((number + 1).toString(), '0', 15, 'TH'),
            number: number + 1,
            date: date,
            time: time,
            description: description,
            department: department,
            file_category: fileCategory,
            file_category_other: fileCategoryOther,
            creature_id: creatureId,
            member_id: user.id,
            status: 1,
            create_by: user.id,
            edit_by: user.id
        }
    },
    generateTreatmentHistoryForUpdateToDb: (params, user) => {
        const { 
            date,
            time,
            description,
            department,
            fileCategory,
            fileCategoryOther } = params

        const result = {
            date: date,
            time: time,
            description: description,
            department: department,
            file_category: fileCategory,
            file_category_other: fileCategoryOther,
            edit_by: user.id,
            edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
        }

        return result
    },
    generateMedicalForInsertToDb: (params, user, number) => {
        const { 
            hospitalName,
            hospitalTel,
            patientId,
            bloodType,
            doctorName,
            doctorContactNo,
            congenitalDisorder,
            creatureId } = params

         return {
            id: uuid(),
            code: sqlHelper.autoFill((number + 1).toString(), '0', 15, 'MD'),
            number: number + 1,
            hospital_name: hospitalName,
            hospital_tel: hospitalTel,
            patient_id: patientId,
            blood_type: bloodType,
            doctor_name: doctorName,
            doctor_contact_no: doctorContactNo,
            congenital_disorder: congenitalDisorder,
            creature_id: creatureId,
            member_id: user.id,
            status: 1,
            create_by: user.id,
            edit_by: user.id
        }
    },
    generateMedicalForUpdateToDb: (params, user) => {
        const { 
            hospitalName,
            hospitalTel,
            patientId,
            bloodType,
            doctorName,
            doctorContactNo,
            congenitalDisorder,
            creatureId } = params

        const result = {
            hospital_name: hospitalName,
            hospital_tel: hospitalTel,
            patient_id: patientId,
            blood_type: bloodType,
            doctor_name: doctorName,
            doctor_contact_no: doctorContactNo,
            congenital_disorder: congenitalDisorder,
            creature_id: creatureId,
            edit_by: user.id,
            edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
        }

        return result
    },
    generateVaccineBookForInsertToDb: (params, user, number) => {
        const { vaccineType, other, creatureId } = params

         return {
            id: uuid(),
            code: sqlHelper.autoFill((number + 1).toString(), '0', 15, 'VB'),
            number: number + 1,
            vaccine_type: vaccineType,
            other: other,
            creature_id: creatureId,
            member_id: user.id,
            status: 1,
            create_by: user.id,
            edit_by: user.id
        }
    },
    generateVaccineBookForUpdateToDb: (params, user) => {
        const { vaccineType, other } = params

        const result = {
            vaccine_type: vaccineType,
            other: other,
            edit_by: user.id,
            edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
        }

        return result
    },
    generateVaccineForInsertToDb: (params, user, number) => {
        const { 
            hospitalName,
            date,
            description,
            vaccineBookId,
            creatureId } = params
        
        return {
            id: uuid(),
            code: sqlHelper.autoFill((number + 1).toString(), '0', 15, 'VC'),
            number: number + 1,
            hospital_name: hospitalName,
            date: date,
            description: description,
            vaccine_book_id: vaccineBookId,
            creature_id: creatureId,
            member_id: user.id,
            status: 1,
            create_by: user.id,
            edit_by: user.id
        }
    },
    generateVaccineForUpdateToDb: (params, user) => {
        const { hospitalName, date, description } = params
        
        const result = {
            hospital_name: hospitalName,
            date: date,
            description: description,
            edit_by: user.id,
            edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
        }

        return result
    },
    generateDrugForInsertToDb: (params, user, number) => {
        const { 
            name,
            description,
            creatureId } = params

         return {
            id: uuid(),
            code: sqlHelper.autoFill((number + 1).toString(), '0', 15, 'DG'),
            number: number + 1,
            name: name,
            description: description,
            creature_id: creatureId,
            member_id: user.id,
            status: 1,
            create_by: user.id,
            edit_by: user.id
        }
    },
    generateDrugForUpdateToDb: (params, user) => {
        const { 
            name,
            description } = params

        const result = {
            name: name,
            description: description,
            edit_by: user.id,
            edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
        }

        return result
    },
    generateDrugAllergyForInsertToDb: (params, user, number) => {
        const { 
            name,
            description,
            creatureId } = params
            
         return {
            id: uuid(),
            code: sqlHelper.autoFill((number + 1).toString(), '0', 15, 'DA'),
            number: number + 1,
            name: name,
            description: description,
            creature_id: creatureId,
            member_id: user.id,
            status: 1,
            create_by: user.id,
            edit_by: user.id
        }
    },
    generateDrugAllergyForUpdateToDb: (params, user) => {
        const { 
            name,
            description } = params

        const result = {
            name: name,
            description: description,
            edit_by: user.id,
            edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
        }

        return result
    },
    generateFoodAllergyForInsertToDb: (params, user, number) => {
        const { 
            name,
            description,
            creatureId } = params
            
         return {
            id: uuid(),
            code: sqlHelper.autoFill((number + 1).toString(), '0', 15, 'FA'),
            number: number + 1,
            name: name,
            description: description,
            creature_id: creatureId,
            member_id: user.id,
            status: 1,
            create_by: user.id,
            edit_by: user.id
        }
    },
    generateFoodAllergyForUpdateToDb: (params, user) => {
        const { 
            name,
            description } = params

        const result = {
            name: name,
            description: description,
            edit_by: user.id,
            edit_date: dayjs().format('YYYY-MM-DTHH:mm:ss.SSSSSS')
        }

        return result
    },
    generateMailForInsertToDb: (params, user) => {
        const { 
            from,
            to,
            subject,
            description,
            html_text,
            tag } = params

        return {
            id: uuid(),
            from: from,
            to: to,
            subject: subject,
            description: description,
            html_text: html_text,
            tag: tag,
            mail_status: 1,
            status: 1,
            // create_by: user.id,
            // edit_by: user.id
        }
    }
}